#include <cuda.h>
#include <cuda_runtime.h>
#include <chrono>
#include <random>
#include <algorithm>
#include <cstring>
#include "data_handler.h"
#include "error_util.h"

data_handler::data_handler(std::string filename,unsigned int batch_s ):batch_size(batch_s)
{
    cnpy::NpyArray arr = cnpy::npy_load(filename);

    if (arr.word_size == sizeof(double))
    {
        arr = convertToFloat(arr);
        split_data_sets<float>(arr);
    }
    else if (arr.word_size == sizeof(float))
        split_data_sets<float>(arr);
    else
        throw std::invalid_argument("Array type not recognized");
    currIdx[Training] = size[Training];
    currIdx[Test] = size[Test];
    currIdx[Validation] = size[Validation];
    normalize();
    //biggest dataset
    checkCudaErrors(cudaStreamCreate(&stream));
    temparr = std::vector<float>(batch_size*seqLength*dataDim);
    data_prepare = thrust::device_vector<float>(batch_s*dataDim*seqLength);
}
cnpy::NpyArray data_handler::convertToFloat(cnpy::NpyArray arr)
{
       std::vector<double> vec = arr.as_vec<double>();
        cnpy::NpyArray ret(arr.shape, sizeof(float), arr.fortran_order);

        std::copy(vec.begin(), vec.end(), ret.data<float>());
     
        return ret;
}
template <typename T>
cnpy::NpyArray data_handler::FortranToC(cnpy::NpyArray arr)
{
	std::vector<T> data = arr.as_vec<T>();
	for (unsigned int i = 0; i<arr.shape[0]; i++)
	{
		for (unsigned int j = 0; j < arr.shape[1]; j++)
		{
			for(unsigned int k = 0; k < arr.shape[2]; k++)
			{
				unsigned int c_map = i*arr.shape[1]*arr.shape[2]+j*arr.shape[2]+k;
				unsigned int f_map = k*arr.shape[0]*arr.shape[1]+j*arr.shape[0]+i;
				arr.data<T>()[c_map] = data[f_map];
			}
		}
	}
	arr.fortran_order = false;
	return arr;
}
template <typename T> 
void data_handler::split_data_sets(cnpy::NpyArray arr)
{
   	seqLength = arr.shape[1];
	dataDim = arr.shape[2];
	size[Test] = arr.shape[0]/10;
	size[Validation] = size[Test];
	size[Training] = arr.shape[0] - size[Test] - size[Validation];

    if (arr.fortran_order)
    {
    	arr = FortranToC<float>(arr);
    }
    size_t vectorLength = seqLength*dataDim;
    int dataSize = arr.shape[0];
    for (int i = 0; i < dataSize; ++i)
    {
    	std::vector<float> vec(vectorLength);
    	std::copy_n(arr.data<float>()+i*vectorLength,vectorLength, vec.begin() );
    	if (size[Training] > data[Training].size())
    		data[Training].push_back(vec);
    	else if (size[Validation] > data[Validation].size() )
    		data[Validation].push_back(vec);
    	else
    		data[Test].push_back(vec);
    }
    //just check if data is properly transmitted
    assert(data[Test].size() == size[Test]);
}
//TODO SAVE Result data function
/*
 *
 */
bool data_handler::next_batch(void* dst, unsigned int seed, setType type)
{
    //shuffle after epoche is finished
    bool epoch_finished = false;
    if (currIdx[type] == size[type])
    {
        //unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937 generator(seed); //initialize random numbers
        std::shuffle(data[type].begin(), data[type].end(), generator);
        currIdx[type] = 0;
        //int size = seqLength*dataDim;
        //int idx = 0;
        /*for (int i = 0; i<size[type]; ++i)
        {
        	thrust::copy_n(data[type][i].begin(), size, data_fused.begin()+idx );
        	idx+=size;
        }*/
    }
    //join the array to send it
    int b_size = batch_size;
    if ((currIdx[type]+batch_size) >= size[type])
        b_size = size[type]-currIdx[type];
    float* vec = temparr.data();
    //float* vec = new float[seqLength*dataDim*b_size];
    /*for (int i = currIdx[type]; i < currIdx[type]+b_size; i++)
    {

    	float* next = static_cast<float*> (dst) + (i-currIdx[type])*dataDim;
    	float* src = thrust::raw_pointer_cast(data[type][i].data());
    	checkCudaErrors(cudaMemcpy2D(next, dataDim*b_size*sizeof(float),
    							src,sizeof(float)*dataDim,
    							dataDim*sizeof(float), seqLength, cudaMemcpyDeviceToDevice));
    }*/

    //copies the values and transposes the tensor from (batchSize, seqLenght, dataDim) to (seqLength, batchSize, dataDim)
    for (unsigned int i = currIdx[type]; i<currIdx[type]+b_size; i++)
    {
    	for (unsigned int j = 0 ; j<seqLength; ++j)
    	{
    		for (unsigned int k = 0; k <dataDim; ++k)
    		{
    			vec[(i-currIdx[type])*dataDim+b_size*dataDim*j+k] = data[type][i][j*dataDim+k];
    		}
    	}
    	/*checkCudaErrors(cudaMemcpy2D(&vec[(i-currIdx[type])*dataDim], sizeof(float)*dataDim*b_size ,
        							&data[type][i][0], sizeof(float)*dataDim,
        							sizeof(float)*dataDim, seqLength, cudaMemcpyHostToHost));*/
    }

    //checkCudaErrors(cudaMemcpyAsync(data_prepare.data().get(), vec, seqLength*dataDim*sizeof(float)*b_size, cudaMemcpyHostToDevice, stream));
    //checkCudaErrors(cudaStreamSynchronize(stream));
    checkCudaErrors(cudaMemcpy(dst, vec, seqLength*dataDim*sizeof(float)*b_size,cudaMemcpyHostToDevice));
    //checkCudaErrors(cudaStreamDestroy(stream));
    currIdx[type] += b_size;
    if (currIdx[type] == size[type]) epoch_finished=true;
    //delete vec;
    return epoch_finished;
}
void data_handler::get_maxmin(std::vector<float>& currMax, std::vector<float>& currMin, setType set)
{
	std::vector<float> data_h(seqLength*dataDim);
	for (int i = 0; i< size[set]; ++i)
	{
		//it's not really smooth, but it's ookay just for data loading purposes
		//thrust::copy(data[set][i].begin(), data[set][i].end(), data_h.begin());
		data_h = data[set][i];
		for (int j = 0; j < dataDim; j++)
		{
			for (int k = j; k < seqLength*dataDim; k += dataDim )
			{
				max[j] = std::max(currMax[j], data_h[k]);
				min[j] = std::min(currMin[j], data_h[k]);
			}
		}
	}
}
void data_handler::normalize()
{
	//TODO FUNCTION NOT FINISHED
	max = std::vector<float>(dataDim,std::numeric_limits<float>::min());
	min = std::vector<float>(dataDim,std::numeric_limits<float>::max());

	get_maxmin(max, min, Training);
	get_maxmin(max, min, Test);
	get_maxmin(max, min, Validation);

	normalizeSet(Training);
	normalizeSet(Test);
	normalizeSet(Validation);

}
void data_handler::normalizeSet(setType set)
{
	std::vector<float> data_h(seqLength*dataDim);
	for (int i = 0; i< size[set]; ++i)
	{
		//it's not really smooth, but it's ookay just for data loading purposes
		//thrust::copy(data[set][i].begin(), data[set][i].end(), data_h.begin());
		data_h = data[set][i];
		for (int j = 0; j< dataDim; ++j)
		{
			for (int k = j ; k< seqLength*dataDim; k +=dataDim)
				//might be missing something
				data_h[k] = (data_h[k]-min[j])/(max[j]-min[j]);
		}
		data[set][i] = data_h;
		//thrust::copy(data_h.begin(), data_h.end(), data[set][i].begin());
	}
}
std::vector<std::vector<float>> data_handler::deNormalizeData(std::vector<std::vector<float>> data)
{
	for (int i = 0; i< data.size(); ++i)
		{
			for (int j = 0; j< dataDim; ++j)
			{
				for (int k = j ; k< data[0].size(); k +=dataDim)
					//might be missing something
					data[i][k] = data[i][k]*(max[j]-min[j]) + min[j];
			}
		}
	return data;
}
void data_handler::savePrediction(void* inData, int inLength, void* predData, int outLength, std::string filename, int _batchSize)
{
	float* inDataf = new float[inLength*dataDim*_batchSize];
	float* predDataf = new float[outLength*dataDim*_batchSize];
	checkCudaErrors(cudaMemcpy(inDataf, inData, inLength*dataDim*sizeof(float)*_batchSize,cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(predDataf, predData, outLength*dataDim*sizeof(float)*_batchSize,cudaMemcpyDeviceToHost));

	std::vector<size_t> predShape = {static_cast<size_t>(outLength), static_cast<size_t>(_batchSize), static_cast<size_t>(dataDim)};
	std::vector<size_t> inShape = {static_cast<size_t>(inLength), static_cast<size_t>(_batchSize), static_cast<size_t>(dataDim)};

	cnpy::npy_save(filename+ "_pred.npy", predDataf, predShape);
	cnpy::npy_save(filename+ "_in.npy", inDataf, inShape );


}
std::vector<unsigned int> data_handler::get_shape() const 
{
    return std::vector<unsigned int>{seqLength, dataDim};
}
std::vector<unsigned int> data_handler::get_setsizes() const
{
	unsigned int trainingSize = size.at(Training);
	unsigned int validSize = size.at(Validation);
	unsigned int testSize = size.at(Test);
    return std::vector<unsigned int>{trainingSize,validSize,testSize} ;
}
size_t data_handler::get_wordsize() const
{
    return sizeof(float);
}
