#include "helper.h"

/*__device__ float helper::sigmoid_f::operator()(float x)
	{
		        		return  1.0/(1.0+exp(-x));
	}
*/
thrust::device_vector<float> helper::linspace(float a, float b, float N)
		{

			float Dx    = (b-a)/(float)(N-1);

			thrust::device_vector<float> myvector(N);

			thrust::transform(thrust::make_counting_iterator(a/Dx),
				 thrust::make_counting_iterator(a/Dx)+N,
                 thrust::make_constant_iterator(Dx),
                 myvector.begin(),
                 thrust::multiplies<float>());
			return myvector;
		}
		void helper::CreateVec(float* hfirstpart, float* hendpart, int hsize, int numLagsfirst, int numLags, int batchSize, float* sstart)
		{
		    dim3 dimGrid((numLags*hsize+TILE_DIM-1)/TILE_DIM, (batchSize+TILE_DIM-1)/TILE_DIM, 1);
            dim3 dimBlock(TILE_DIM, TILE_DIM, 1);
            CreateVecCore<<<dimGrid, dimBlock>>>(hfirstpart,hendpart, hsize, numLagsfirst, numLags, batchSize, sstart);
		}
		__global__ void helper::CreateVecCore(float* hfirstpart, float* hendpart, int hsize, int numLagsfirst, int numLags, int batchSize, float* sstart)
		{
		    __shared__ float tile[TILE_DIM][TILE_DIM];
            int x = blockIdx.x * TILE_DIM + threadIdx.x;
            int y = blockIdx.y * TILE_DIM + threadIdx.y;
            int lag = x / hsize;
            if ((y < batchSize) && (x < hsize*numLags))
            {
                if (lag<numLagsfirst)
                {
                    tile[threadIdx.y][threadIdx.x] = hfirstpart[lag*hsize*batchSize+x-lag*hsize+ y*hsize];
                     //printf("Print valfirst %f at %d %d from %d\n", hfirstpart[lag*hsize*batchSize+x-lag*hsize+ y*hsize], threadIdx.y, threadIdx.x, lag*hsize*batchSize+x-lag*hsize+ y*hsize);
                }
                else
                {
                    tile[threadIdx.y][threadIdx.x] = hendpart[x-lag*hsize+ (lag-numLagsfirst)*hsize*batchSize+ y*hsize];
                    //printf("Print valsecond  %f\n", hendpart[x-(lag-numLagsfirst)*hsize+ y*hsize]);
                }
                 __syncthreads();
                //printf("Save %d %d to %d \n", threadIdx.y, threadIdx.x, 1+y*(1+numLags*hsize)+(numLags-1-lag)*hsize+x-lag*hsize);
                sstart[1+y*(1+numLags*hsize)+(numLags-1-lag)*hsize+x-lag*hsize] = tile[threadIdx.y][threadIdx.x];
            }    
		}
