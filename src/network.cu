#include "network.h"
#include "error_util.h"
#include <cublas_v2.h>
#include "RNN.h"
#include "TTLSTM.h"
#include "TTRNN.h"

network::unit_t::unit_t(int _num_layer,
        int hiddenSize,
        int num_lags,
        int numOrders,
        network_type net_type,
        int inputSize,
        int outputSize,
        activationType actOut,
        activationType actHidden,
        std::vector<int> _TT_ranks /*= std::vector<int>()*/):
        numLayers(_num_layer)
{
	switch(net_type)
	{
		case RNN:
			if (numLayers == 1)
			{
				layers.push_back(new RNN_t(inputSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden));
			}
			else
			{
				layers.push_back(new RNN_t(inputSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden));
			}
			for (int i = 1; i<numLayers-1; i++)
			{
				layers.push_back(new RNN_t(hiddenSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden));
			}
			layers.push_back(new RNN_t(hiddenSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden));
			break;
		case TTRNN:
			if (numLayers == 1)
			{
				layers.push_back(new RNNTT_t(inputSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			else
			{
				layers.push_back(new RNNTT_t(inputSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			for (int i = 1; i<numLayers-1; i++)
			{
				layers.push_back(new RNNTT_t(hiddenSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			layers.push_back(new RNNTT_t(hiddenSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			break;
		case TTLSTM:
			if (numLayers == 1)
			{
				layers.push_back(new LSTMTT_t(inputSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			else
			{
				layers.push_back(new LSTMTT_t(inputSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			for (int i = 1; i<numLayers-1; i++)
			{
				layers.push_back(new LSTMTT_t(hiddenSize,hiddenSize,0,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			}
			layers.push_back(new LSTMTT_t(hiddenSize,hiddenSize,outputSize,num_lags,numOrders, actOut, actHidden, _TT_ranks));
			break;
	}
}
network::network(int _num_layer,
        int _hidden_size,
        int _num_lags,
        int _numOrders,
        network_type _net_type,
        int _inputSize,
        int _outputSize,
        activationType actOut,
        activationType actHidden,
        std::vector<int> _TT_ranks /*= std::vector<int>()*/):
        numLayers(_num_layer),
        hiddenSize(_hidden_size),
        TT_ranks(_TT_ranks),
        numLags(_num_lags),
        numOrders(_numOrders),
        net_type(_net_type),
        inputSize(_inputSize),
        outputSize(_outputSize),
        batchSize(0),
        stepNum(0),
        encoder(_num_layer,_hidden_size,_num_lags,_numOrders,_net_type, _inputSize, 0, actOut, actHidden, _TT_ranks),
		decoder(_num_layer,_hidden_size,_num_lags,_numOrders,_net_type, _inputSize, _outputSize, actOut, actHidden, _TT_ranks)
{
	checkCublasErrors(cublasCreate(&cublasHandle));

}
void network::unit_t::Init(matop::matSequence_t* dataIn, matop::matSequence_t*& dataOut, matop::matSequence_t*&dataGrad, int _batchSize, bool isTraining,int seqLength)
{
	for (int i = 0; i <numLayers; ++i)
	{
		layers[i]->Init(_batchSize, dataIn, dataGrad, isTraining, seqLength);
	}
	dataOut = dataIn;
}
matop::matSequence_t* network::Init(matop::matSequence_t* dataIn, int _batchSize, bool isTraining,int _inSteps, int _outSteps)
{
	matop::matSequence_t* dataGrad = nullptr;
	matop::matSequence_t* dataOut = nullptr;
	inData = dataIn;
	inpSteps = _inSteps;
	outSteps = _outSteps;
	batchSize = _batchSize;
	layer_t::initStaticHelpers(numLags, hiddenSize,  batchSize,  numOrders, (net_type == RNN), (net_type == TTRNN),(net_type ==TTLSTM), TT_ranks);

	encoder.Init(dataIn, dataOut, dataGrad, batchSize, isTraining, _inSteps);
	dataGrad = nullptr;
	dataIn = nullptr;
	dataOut = nullptr;

	decoder.Init(dataIn, dataOut, dataGrad, batchSize, isTraining, _outSteps);
	outData = dataOut;
	decoder.SetInput(dataOut, dataGrad);
	loss = dataGrad;//thrust::device_vector<float> (outputSize*batchSize*seqLength,0);
	dataOut = outData;
	stepNum = 0;
	return dataOut;
}
/*size_t network::unit_t::GetMemInfo()
{
	size_t memsize = 0;
	for (int i = 0; i< numLayers; ++i)
	{
		memsize += layers[i]->GetMemInfo();
	}
	return memsize;
}
size_t network::GetMemInfo()
{
	return encoder.GetMemInfo()+decoder.GetMemInfo();
}*/

void network::unit_t::resetStates()
{
	for (int i=0; i < numLayers; ++i)
	{
		layers[i]->resetStates();
	}
}
void network::unit_t::resetStateGrad()
{
	for (int i=0; i < numLayers; ++i)
	{
		layers[i]->resetStateGrad();
	}
}
//function for decoder
void network::unit_t::ForwardTraining(const cublasHandle_t& cublasHandle, int seqStart, int seqEnd)
{
	for (int j = seqStart; j< seqEnd; ++j)
		for (int i = 0; i< numLayers; ++i)
		{
			layers[i]->ForwardTraining(cublasHandle, j);
		}

}
float network::Train(thrust::device_ptr<float> targetValues, float stepSize)
{
	this->Forward();
	LossGradient(targetValues,0, outSteps/*trainingSeqLength*/);
	float loss = Loss(cublasHandle);
	this->Backward();

    stepNum++;
	this->UpdateWeights(stepSize, stepNum);

	return loss;
}
void network::Forward()
{
	if (net_type == TTLSTM)
	{
		std::vector<RNNState> encoderStates = encoder.getLastState(inpSteps-1, numLags); //-1 because of zero based indexing
		std::vector<MemState> encoderMem = encoder.getLastMemory(inpSteps-1);

		encoder.resetStates();

		encoder.ForwardTraining(cublasHandle, 0, inpSteps/*trainingSeqLength*/);
		decoder.setInitialStates(encoderStates);
		decoder.setInitialMemory(encoderMem);
		decoder.ForwardTraining(cublasHandle, 0, outSteps/*trainingSeqLength*/);
	}
	else
	{
		std::vector<RNNState> encoderStates = encoder.getLastState(inpSteps-1, numLags); //-1 because of zero based indexing

		encoder.resetStates();

		encoder.ForwardTraining(cublasHandle, 0, inpSteps/*trainingSeqLength*/);
		decoder.setInitialStates(encoderStates);
		decoder.ForwardTraining(cublasHandle, 0, outSteps/*trainingSeqLength*/);
	}
}
void network::UpdateWeights(float stepSize, int stepNum)
{
	decoder.UpdateWeights(stepSize, stepNum);
	encoder.UpdateWeights(stepSize, stepNum);
}
void network::Backward()
{
	if (net_type == TTLSTM)
	{
		encoder.resetStateGrad();
		decoder.resetStateGrad();

		std::vector<RNNState> statesGrad = decoder.getLastStateGrad(-1, numLags); //if seq num = 0 the last operation in layer->backward calculates dh[-1] to dh[-numLags
		std::vector<MemState> memgrad = decoder.getLastMemoryGrad(-1);

		decoder.Backward(cublasHandle);
		encoder.setInitialStateGrad(statesGrad,/*trainingSeqLength*/inpSteps-1); //-1 cause we are zero based
		encoder.setInitialMemoryGrad(memgrad);
		encoder.Backward(cublasHandle);
	}
	else
	{
		encoder.resetStateGrad();
		decoder.resetStateGrad();

		std::vector<RNNState> statesGrad = decoder.getLastStateGrad(-1, numLags); //if seq num = 0 the last operation in layer->backward calculates dh[-1] to dh[-numLags


		decoder.Backward(cublasHandle);
		encoder.setInitialStateGrad(statesGrad,/*trainingSeqLength*/inpSteps-1); //-1 cause we are zero based
		encoder.Backward(cublasHandle);
	}

}
float network::Validate(thrust::device_ptr<float> targetValue)
{
	this->Forward();

	LossGradient(targetValue,0, outSteps/*trainingSeqLength*/);
	return Loss(cublasHandle);
}
void network::unit_t::Backward(const cublasHandle_t& cublasHandle)
{
	//if (outputSize > 0) layers.back()->ActivationGradientOutput(loss);
	for (int i = numLayers-1; i>=0; --i)
	{
		layers[i]->BackwardTraining(cublasHandle);
	}
}
void network::LossGradient(thrust::device_ptr<float> targetValues, int seqStart, int seqEnd)
{
	int outSize = outputSize*batchSize;
	thrust::transform((*outData)[1].data(),(*outData)[1].data()+outSize*(seqEnd-seqStart),
						targetValues+outSize*seqStart, (*loss)[1].data(), thrust::minus<float>() ); //offset becasue the layer saves to y+1
	/*thrust::transform((*outData)[0], (*outData)[0]+outSize,
						targetValues+outSize*(seqEnd-1), (*loss)[0], thrust::minus<float>() );*/
}
float network::Loss(const cublasHandle_t& cublasHandle)
{
	float result = 0;
	checkCublasErrors(cublasSnrm2(cublasHandle, outSteps*batchSize*outputSize, thrust::raw_pointer_cast((*loss)[1].data()), 1, &result ));
	return sqrt(pow(result,2)/(float)(outputSize*batchSize*outSteps));
}
std::vector<RNNState> network::unit_t::getLastState(int seqNum, int _numLags) const
{
	std::vector<RNNState>states;
	for (int i = 0; i <numLayers; ++i)
	{
		states.push_back(layers[i]->getLastStates(seqNum, _numLags));
	}
	return states;
}
void network::unit_t::setInitialStates(std::vector<RNNState> states)
{
	for (int i = 0; i < numLayers; ++i)
	{
		layers[i]->setInitialState(states[i]);
	}

}
void network::unit_t::setInitialStateGrad(std::vector<RNNState> states,int seqNum)
{
	for (int i = 0; i < numLayers; ++i)
	{
		layers[i]->setInitialStateGrad(states[i], seqNum);
	}

}
std::vector<RNNState> network::unit_t::getLastStateGrad(int seqNum, int _numLags) const
{
	std::vector<RNNState> states;
	for (int i = 0; i <numLayers; ++i)
	{
		states.push_back(layers[i]->getLastStatesGrad(seqNum, _numLags));
	}
	return states;
}
void network::unit_t::setInitialMemoryGrad(std::vector<MemState> states)
{
	for (int i = 0; i < numLayers; ++i)
	{
			layers[i]->setInitialMemoryGrad(states[i]);
	}
}
std::vector<MemState> network::unit_t::getLastMemoryGrad(int seqNum) const
{
	std::vector<MemState> states;
	for (int i = 0; i <numLayers; ++i)
	{
		states.push_back(layers[i]->getLastMemoryGrad(seqNum));
	}
	return states;
}
std::vector<MemState> network::unit_t::getLastMemory(int seqNum) const
{
	std::vector<MemState>states;
	for (int i = 0; i <numLayers; ++i)
	{
		states.push_back(layers[i]->getLastMemory(seqNum));
	}
	return states;
}
void network::unit_t::setInitialMemory(std::vector<MemState> states)
{
	for (int i = 0; i < numLayers; ++i)
		{
			layers[i]->setInitialMemory(states[i]);
		}
}
void network::unit_t::SetInput(matop::matSequence_t* input, matop::matSequence_t* inputgrad)
{
	//inData = input;
	layers[0]->setInput(input, inputgrad);
}
/*int network::getTensorSize()
{
	return layers[0]->getTensorSize();
}*/
void network::unit_t::UpdateWeights(float learningRate, int stepNum)
{
	for (int i = 0; i< numLayers; ++i)
	{
		//std::vector<float> vec = layers[i]->weightDistribution();
		//std::cout << "Layer "<< i << " min weight: " << vec[0] << " max weight: " << vec[1] << std::endl;
		layers[i]->UpdateWeights(learningRate, stepNum);
	}
}
network::~network()
{
	layer_t::destroyHelper();
}
void network::unit_t::SetWeights(matop::matrix_t _Wx, matop::matrix_t _Wy, matop::matrix_t _Wh, matop::matrix_t _by )
{
	for (int i = 0; i<numLayers; i++)
	{
		layers[i]->SetWeights(_Wx,_Wy, _Wh, _by);
	}
}
void network::unit_t::GetWeightGrads(matop::matrix_t& _Wx, matop::matrix_t& _Wy, matop::matrix_t& _Wh , int layerNum)
{
	assert(layerNum < numLayers && layerNum >=0);
	layers[layerNum]->GetWeightGrad(_Wx, _Wy, _Wh);
}
