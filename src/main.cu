/*
 * main.cu

 *
 *  Created on: Jun 14, 2020
 *      Author: julian
 */
#include "data_handler.h"
#include "network.h"
#include <assert.h>
#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <set>
#include "error_util.h"
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <chrono>
#include <thrust/logical.h>
#include "helper.h"
#include "matop.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main(int argc, char **argv)
{
	int hiddenSize = 0;
	int numLags = 0;
	int numOrders =0;
	int batchSize = 0;
	int TT_rank = 0;
	int numLayers = 0;
	int inpSteps = 0;
	int outSteps = -1;
	float learningRate;

	int trainingSteps = 20000;
	int displayStep = 500;

	bool LSTM = false;

	std::string filename(argv[1]);
	std::string outName(argv[2]);
	
	std::vector<int> TT_ranks;

	 int c;

	opterr = 0;


	while ((c = getopt (argc, argv, "s:h:l:o:b:r:nx:y:a:m:d:")) != -1)
	{
		switch(c)
		{
			case 's':
				hiddenSize = std::stoi(optarg);
				break;
			case 'h':
				numLags = std::stoi(optarg);
				break;
			case 'l':
				numLayers = std::stoi(optarg);
				break;
			case 'o':
				numOrders = std::stoi(optarg);
				break;
			case 'b':
				batchSize = std::stoi(optarg);
				break;
			case 'r':
			{
			    std::string ranks(optarg);
			    std::string delimiter(",");
			    size_t pos = 0;
                std::string token;
                while ((pos = ranks.find(delimiter)) != std::string::npos) {
                    token = ranks.substr(0, pos);
                    TT_rank = std::stoi(token);
                    TT_ranks.push_back(TT_rank);
                    ranks.erase(0, pos + delimiter.length());
                }
                TT_ranks.push_back(std::stoi(ranks));
				break;
				}
			case 'n':
				LSTM = true;
				break;
			case 'x':
				inpSteps = std::stoi(optarg);
				break;
			case 'y':
				outSteps = std::stoi(optarg);
				break;
			case 'a':
				learningRate = std::stof(optarg);
				break;
			case 'm':
				trainingSteps = std::stof(optarg);
				break;
			case 'd':
				displayStep = std::stof(optarg);
				break;
			case '?':
				if (optopt == 'c')
					fprintf (stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr,
							"Unknown option character `\\x%x'.\n",
							optopt);
				return 1;
			default:
				abort ();
		}
	}



		data_handler TrainingData(filename, batchSize);
		int totalSequenceLength = TrainingData.get_shape()[0];
		int dataDim = TrainingData.get_shape()[1];
		outSteps = (outSteps==-1) ? totalSequenceLength - inpSteps : outSteps;
        
        assert(numOrders-1 == TT_ranks.size());
		//std::vector<int> TT_ranks(numOrders-1,TT_rank);

		network network(numLayers, hiddenSize, numLags, numOrders, (LSTM)? TTLSTM : TTRNN, dataDim, dataDim,Linear, TanH, TT_ranks);

        matop::matSequence_t x_s;
        thrust::device_vector<float> x_data;
        layer_t::InitDataAndMatrix(matop::shape_t{batchSize, dataDim}, totalSequenceLength, x_data, x_s);
		matop::matSequence_t* x = &x_s;
		float* x_ptr = thrust::raw_pointer_cast(x_s.data());
		thrust::device_ptr<float> z = x_s[inpSteps].data();
		bool isTraining = true;

		matop::matSequence_t* predData = network.Init(x, batchSize, isTraining, inpSteps, outSteps);

		size_t free = 0;
		size_t total = 0;
		checkCudaErrors(cudaDeviceSynchronize());
		cudaMemGetInfo(&free, &total);

		std::cout << "Memory used "<< (float)(total-free)/total*100 << "%" << " Absolute " << (total-free)/ 1e6 << "MB" <<   std::endl;

		float stepSizeInit = learningRate;
		float decayRate = 1.0;
		float stepSize = 0;

		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

        cudaEventRecord(start);
		//decoder.SetInput(y);
		float result = 0;
		float validation = 0;
		for (int i =0; i<trainingSteps; i++)
		{

			stepSize = stepSizeInit * pow(decayRate, i/2000);
			TrainingData.next_batch((void*) x_ptr, std::chrono::system_clock::now().time_since_epoch().count(), Training); //next batch should give (seqLength, batchSize, dataDim)

			result += network.Train(z, stepSize);
			
			bool validEnd = false;

		    validEnd = TrainingData.next_batch((void*) x_ptr, std::chrono::system_clock::now().time_since_epoch().count(), Validation);
			validation += network.Validate(z);

			if ((i+1) % displayStep == 0)
			{
				float miliseconds = 0;
				float time = 0;
				cudaEventRecord(stop);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&miliseconds, start, stop);
				time = miliseconds/(i);
				std::cout << "Step " << i << " ------> ";
				std::cout << "Loss on Trainingset " << result/displayStep  << std::endl;
				std::cout << "\tLoss on Validationset " << validation/displayStep << std::endl;
				std::cout << "--->Learning Rate " << stepSize;
				std::cout << "--->Average Time per Run " << time << "ms" << std::endl;
				result = 0;
				validation = 0;
			}


		}
		bool testEnd = false;
		result = 0;
		int i = 0;
		while (!testEnd)
		{
		    i++;
		    testEnd = TrainingData.next_batch((void*) x_ptr, std::chrono::system_clock::now().time_since_epoch().count(), Test); //next batch should give (seqLength, batchSize, dataDim)
		    result += network.Validate(z);
		}
		std::cout << "Loss on TestSet" << std::endl;
		std::cout << "Loss " << result/i << std::endl ;

		TrainingData.savePrediction(x_ptr, inpSteps+outSteps, (*predData)[1].ptr(), outSteps, outName, batchSize);
		//cudaDeviceReset();

}



