/*
 * RNN.cu

 *
 *  Created on: Jul 9, 2020
 *      Author: julian
 */
#include "RNN.h"
#include "helper.h"
#include "error_util.h"
#include "matop.h"
RNN_t::RNN_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut , activationType _actTypeHid):
		layer_t( _inputNeurons,  _hiddenNeurons,_outputNeurons, _numLags,_numOrders, _actTypeOut , _actTypeHid)
{
	layer_t::InitDataAndMatrix(matop::shape_t{higherOrderStateSize, hiddenNeurons}, wh_data, wh);
	//wh = thrust::device_vector<float>(hiddenNeurons*higherOrderStateSize);
	thrust::generate(wh.begin(),wh.end(), helper::xavier_init(higherOrderStateSize,std::chrono::system_clock::now().time_since_epoch().count()));
}

void RNN_t::StateGradient(const cublasHandle_t& cublasHandle,const matop::matrix_t& stateTensorGrad,const matop::matrix_t& _sgrad, int seqNum)
{
	if (numOrders == 1)
	{
		thrust::copy(stateTensorGrad.begin(), stateTensorGrad.end(), _sgrad.begin());
	}
	else
	{

		//the vector of  acts like a circular list if I access the indices with (idx%numOrders)
		std::vector<matop::matrix_t> multiplicationList(numOrders, matop::matrix_t(s.data(),s.cols,1));
		multiplicationList[0] = matop::matrix_t(ds.data(), ds.rows*ds.cols,1,1,0);

		//unfolding on second dimension to make it a vector
		//matop::matrixBatched_t gradBatched = grad.toMatBatched(numOrders-1);

		//matop::tensor_t<float> result = (numOrders == 2) ? grad2 : grad;

		for (int i= 0; i< numOrders; i++)
		{
			size_t rows = multiplicationList[i%numOrders].rows;
			size_t cols = multiplicationList[(i+1)%numOrders].rows;
			size_t stride = rows*cols;

			if (numOrders == 2)
			{
				matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
						1.f, rows, 1, cols,
						multiplicationList[i%numOrders].data(), 1, multiplicationList[i%numOrders].stride,
						multiplicationList[(i+1)%numOrders].data(), 1, multiplicationList[(i+1)%numOrders].stride,
						(i >0 ) ? 1.f : 0.f,
						grad2.data(), cols, stride,
						batchSize);
			}
			else
			{
				matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
							1.f, rows, 1, cols,
							multiplicationList[i%numOrders].data(),multiplicationList[i%numOrders].ld, multiplicationList[i%numOrders].stride,
							multiplicationList[(i+1)%numOrders].data(),multiplicationList[(i+1)%numOrders].ld, multiplicationList[(i+1)%numOrders].stride,
							0,
							grad.data(), cols,stride,
							batchSize);
			}

			for (int j = 2; j < numOrders; j++)
			{
				rows = stride;
				cols = multiplicationList[(i+j)%numOrders].rows;
				stride = rows*cols;
				if (j < numOrders-1)
				{
				//matResult =  takeMatrices((j == numOrders-1) ? grad2.toMatBatched(0) : factorA, 0, factorA.rows,multiplicationList[(i+j)%numOrders].rows) ;
					matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
						1.f, rows, 1, cols,
						grad.data(), 1, rows,
						multiplicationList[(i+j)%numOrders].data(), multiplicationList[(i+j)%numOrders].ld,multiplicationList[(i+j)%numOrders].stride,
						0,
						grad.data(), cols, stride,
						batchSize);
				}
				else
				{
					matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
											1.f, rows, 1, cols,
											grad.data(), 1, rows,
											multiplicationList[(i+j)%numOrders].data(), multiplicationList[(i+j)%numOrders].ld,  multiplicationList[(i+j)%numOrders].stride,
											(j == numOrders-1 && i >0)? 1:0,
											grad2.data(), cols, stride,
											batchSize);
				}

			}

		}
		matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
				1.f, stateVectorSize, higherOrderStateSize, 1,
				grad2.data(), stateVectorSize, stateVectorSize*higherOrderStateSize,
				stateTensorGrad.data(), 1, higherOrderStateSize,
				0.f,
				_sgrad.data(), 1, _sgrad.ld,
				batchSize
				);
		}


}

void RNN_t::ForwardState(const cublasHandle_t& cublasHandle, const matop::matrix_t& stateInput, matop::matrix_t& stateOutput)
{
	CreateStateTensor(cublasHandle, stateInput, stateTensor);
	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
			1.f,
			stateTensor,
			wh,
			1.f,
			stateOutput);

}

void RNN_t::BackwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& _s,const matop::matrix_t& stateGrad, matop::matrix_t& _sgrad)
{

	CreateStateTensor(cublasHandle, _s, stateTensor);
	matop::GEMM(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
			1.f,
			stateTensor,
			stateGrad,
			1.f,
			dwh);

	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
			1.f,
			stateGrad,
			wh,
			0.f,
			stateTensor);

	StateGradient(cublasHandle, stateTensor, _sgrad, seqCounter-1);
}

void RNN_t::CreateStateTensor(const cublasHandle_t& cublasHandle, const matop::matrix_t& _s, matop::matrix_t& _stateTensor)
{
	//reset statetensor
	thrust::fill(_stateTensor.begin(), _stateTensor.end(), 0);

	/*matop::matrixBatched_t tensor = _stateTensor.toMatBatched(numOrders-1);
	matop::matrixBatched_t vector = _s.toMatBatched(0);*/



	checkCudaErrors(cudaMemcpy2D(_stateTensor.ptr(), _s.cols*sizeof(float),
								_s.ptr(), _s.cols*sizeof(float), _s.cols*sizeof(float), batchSize,
								cudaMemcpyDeviceToDevice));

	size_t rows = _s.cols;
	size_t cols = _s.cols;
	size_t stride = rows*cols;
	for (int i = 0; i<numOrders-1; i++)
	{
		matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
				1.f, rows, 1, cols,
				_stateTensor.data(), 1, rows,
				_s.data(), 1, cols,
				0.f,
				_stateTensor.data(), cols, stride,
				batchSize);

		rows = stride;
		stride *=cols;
	}

}



