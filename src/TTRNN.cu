/*
 * TTRNN.cu
 *
 *  Created on: Jun 20, 2020
 *      Author: julian
 */
#include "TTRNN.h"
#include "helper.h"
#include "matop.h"
int RNNTT_t::TTStride(int coreNum)
{
	int result = 0;
	for (int i = 0; i< coreNum; ++i)
	{
		result += stateVectorSize*TT_ranks[i]*TT_ranks[i+1];
	}
	return result;
}
void RNNTT_t::ForwardState(const cublasHandle_t& cublasHandle, const matop::matrix_t& stateInput, matop::matrix_t& stateOutput)
{
	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
			1.f,
			stateInput, //stateInput.toMat(0)
			wh,//wh.toMat(0),
			(numOrders > 1) ? 0.f : 1.f,
			(numOrders > 1) ? TTStates[seqCounter]: stateOutput);
				//TTStates[seqCounter].toMat(0));

	int startStride = TT_ranks[0]*TT_ranks[1];
	if (numOrders > 1)
	{
	matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
											1.f, TT_ranks[0], TT_ranks[1], TT_ranks[2],
											TTStates[seqCounter].data(), TT_ranks[1], TTRanksStacked,
											TTStates[seqCounter].data()+startStride, TT_ranks[2], TTRanksStacked,
											(numOrders > 2) ? 0 :1,
											(numOrders > 2) ? TTSubResults[0][seqCounter].data(): stateOutput.data(), TT_ranks[2], TT_ranks[2],
													batchSize);
													}
	for (int i=1; i<numOrders-1; ++i)
	{
		startStride += TT_ranks[i]*TT_ranks[i+1];
		matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
							1.f, TT_ranks[0], TT_ranks[i+1], TT_ranks[i+2],
							TTSubResults[i-1][seqCounter].data(), TT_ranks[i+1], TT_ranks[i+1],
							TTStates[seqCounter].data()+startStride, TT_ranks[i+2], TTRanksStacked,
							(i < numOrders-2) ? 0 :1,
							(i < numOrders-2) ? TTSubResults[i][seqCounter].data(): stateOutput.data(), TT_ranks[i+2], TT_ranks[i+2],
									batchSize);
	}
}
void RNNTT_t::BackwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& _s,const matop::matrix_t& stateGrad, matop::matrix_t& _sgrad)
{
	/* getSubResults() begin*/
	int startStride = TTRanksStacked;
	for (int i=numOrders-2; i>=0; --i )
	{
		startStride -= TT_ranks[i+1]*TT_ranks[i+2];
		if (i > 0)
		{
			matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
							1.f, TT_ranks[i+1], TT_ranks[0], TT_ranks[i+2],
							TTSubResults[i-1][seqCounter].data(),TT_ranks[i+1], TT_ranks[i+1],
							(i == numOrders-2) ? stateGrad.data() : TTSubResults[i][seqCounter].data(), TT_ranks[i+2], TT_ranks[i+2],
							0.f,
							dsubTensors.data()+startStride, TT_ranks[i+2] ,TTRanksStacked,
							batchSize);
			matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
						1.f, TT_ranks[0], TT_ranks[i+2], TT_ranks[i+1],
						(i == numOrders-2) ? stateGrad.data() : TTSubResults[i][seqCounter].data(), TT_ranks[i+2], TT_ranks[i+2],
						TTStates[seqCounter].data()+startStride, TT_ranks[i+2], TTRanksStacked,
						0.f,
						TTSubResults[i-1][seqCounter].data(), TT_ranks[i+1], TT_ranks[i+1],
						batchSize);
		}
		else
		{
			matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
										1.f, TT_ranks[i+1], TT_ranks[0], TT_ranks[i+2],
										TTStates[seqCounter].data(),TT_ranks[i+1], TTRanksStacked,
										(i == numOrders-2) ? stateGrad.data() : TTSubResults[i][seqCounter].data(), TT_ranks[i+2], TT_ranks[i+2],
										0.f,
										dsubTensors.data()+startStride, TT_ranks[i+2] ,TTRanksStacked,
										batchSize);
			matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
									1.f, TT_ranks[0], TT_ranks[i+2], TT_ranks[i+1],
									(i == numOrders-2) ? stateGrad.data() : TTSubResults[i][seqCounter].data(), TT_ranks[i+2], TT_ranks[i+2],
									TTStates[seqCounter].data()+startStride, TT_ranks[i+2], TTRanksStacked,
									0.f,
									dsubTensors.data(), TT_ranks[i+1], TTRanksStacked,
									batchSize);
		}
	}
	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
				1.f,
				(numOrders > 1) ? dsubTensors : stateGrad,
				wh,
				0.f,
				_sgrad);
	matop::GEMM(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
				1.f,
				_s,
				(numOrders > 1) ? dsubTensors : stateGrad,
				1.f,
				dwh);
}

RNNTT_t::RNNTT_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut, activationType _actTypeHid, std::vector<int> _TT_ranks):
								TT_ranks(_TT_ranks), layer_t(_inputNeurons, _hiddenNeurons, _outputNeurons,  _numLags, _numOrders, _actTypeOut , _actTypeHid)
{
	TT_ranks.insert(TT_ranks.begin(), 1);
	assert(TT_ranks.size() == numOrders);
	TT_ranks.push_back(hiddenNeurons);
	TTRanksStacked = 0;
	for (int i = 0; i< TT_ranks.size()-1; ++i)
	{
		TTRanksStacked += TT_ranks[i]*TT_ranks[i+1];
	}
	layer_t::InitDataAndMatrix(matop::shape_t{stateVectorSize, TTRanksStacked}, wh_data, wh);
	int startStride = 0;
	for (int i = 0; i<numOrders; i++)
	{
		for (int j = 0; j < stateVectorSize; j++) //memory layout is (stateVectorSize, TTRanksStacked) with each kernel of size (stateVectorSize, R_i, R_(i+1))
		{
		thrust::generate(wh.begin()+ j*TTRanksStacked + startStride,wh.begin()+ j*TTRanksStacked + startStride + TT_ranks[i]*TT_ranks[i+1] ,
				helper::glorot_init(stateVectorSize, TT_ranks[i], TT_ranks[i+1],std::chrono::system_clock::now().time_since_epoch().count()));
		}
		startStride += TT_ranks[i]*TT_ranks[i+1];
	}
	//std::cout << "matr " << wx.data().get() << "\t"<< wx.size()<< "\t"<< wx.data().get()+wx.size()<<std::endl;
	//std::cout << "matr " << wh.data().get() << "\t"<< wh.size()<< "\t" << wh.data().get()+wh.size()<<std::endl;
}
void RNNTT_t::InitLayerSpecific()
{
	layer_t::InitLayerSpecific();
	layer_t::InitDataAndMatrix(matop::shape_t{batchSize, TTRanksStacked},seqLength, TTStates_data, TTStates);

	int size = (numOrders >1 ) ? std::accumulate(TT_ranks.begin()+2, TT_ranks.end()-1,0): 0; //since TT_rank[0] is one the size is just the addition of all ranks
	int startOffset = 0;
	TTSubResults_data = thrust::device_vector<float>(batchSize*seqLength*size); //(numOrders-2) arrays, each of size batchSize*seqLength*TT_ranks[i+2]
	for (int i = 0; i< numOrders-2; i++)
	{
		TTSubResults.push_back(matop::matSequence_t(TTSubResults_data.data()+startOffset,matop::shape_t{batchSize,TT_ranks[0]*TT_ranks[i+2]},seqLength));
		//TTSubResults_data.push_back(thrust::device_vector<float>());
		//TTSubResults.push_back(matop::matSequence_t());

		startOffset += TTSubResults[i].size();

		//layer_t::InitDataAndMatrix(matop::shape_t{batchSize,TT_ranks[0]*TT_ranks[i+2]}, seqLength, TTSubResults_data[i], TTSubResults[i]);
		//std::cout << "data " << TTSubResults_data[i].data().get() << std::endl;
		//std::cout << "matr " << TTSubResults[i].data().get() << std::endl;

	}

}



