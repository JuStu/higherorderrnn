/*
 * CUBLAS.cu
Contains wrappers for CublasFunctions to use C-Style arrays more easily
 *
 *  Created on: Jun 10, 2020
 *      Author: julian
 */
//Input array can be in C-Style and the function handles it correctly like it is used in C
#include "matop.h"
#include "error_util.h"
#include <cublas_v2.h>
#include <thrust/device_vector.h>


void matop::MatAdd(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const int m, const int n,
				const float alpha,
				const thrust::device_ptr<float>& A, const int Ald,
				const thrust::device_ptr<float>& B, const int Bld,
				const float beta,
				thrust::device_ptr<float> C, const int Cld)
{
	checkCublasErrors(cublasSgeam(cublasHandle,
				                         transa,  transb,
				                          n, m,
				                          &alpha,
				                          A.get(), Ald,
				                          &beta,
				                          B.get(), Bld,
				                          C.get(), Cld));
}
void matop::GEMM_StridedBatched(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const float           alpha, const int m, const int k, const int n,
				const thrust::device_ptr<float> A, const int lda, const int stridea,
				const thrust::device_ptr<float> B, const int ldb, const int strideb,
				const float           beta,
				thrust::device_ptr<float>       C, const int ldc, const int stridec,
				const int batchSize)
{
		checkCublasErrors(cublasSgemmStridedBatched(cublasHandle, transb, transa,n,m,k, &alpha, B.get(), ldb/*n*/, strideb, A.get(), lda/*k*/, stridea, &beta, C.get(), ldc, stridec, batchSize ));

}
void matop::MatAdd(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
			const float alpha,
			const matrix_t& A,
			const matrix_t& B,
			const float beta,
			matrix_t& C)
{
	int bcols = B.rows;
	int brows = B.cols;
	int acols = A.rows;
	int arows = A.cols;
	if (transb == CUBLAS_OP_T)
	{
		bcols = B.cols;
		brows = B.rows;
	}
	if (transa == CUBLAS_OP_T)
	{
		acols = A.cols;
		arows = A.rows;
	}
	assert(arows == brows);
	assert(acols == bcols);

	checkCublasErrors(cublasSgeam(cublasHandle,
			                         transa,  transb,
			                          arows, acols,
			                          &alpha,
			                          A.ptr(), A.ld,
			                          &beta,
			                          B.ptr(), B.ld,
			                          C.ptr(), C.ld));

}

void matop::GEMM(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const float           alpha,
				const matrix_t& 	A,
				const matrix_t& 	B,
				const float           beta,
				matrix_t&          C)
{
	int bcols = B.rows;
	int brows = B.cols;
	int acols = A.rows;
	int arows = A.cols;
	if (transb == CUBLAS_OP_T)
	{
		bcols = B.cols;
		brows = B.rows;
	}
	if (transa == CUBLAS_OP_T)
	{
		acols = A.cols;
		arows = A.rows;
	}
	assert(arows == bcols);
	checkCublasErrors(cublasSgemm(cublasHandle, transb, transa,brows,acols,arows, &alpha, B.ptr(), B.ld/*n*/, A.ptr(), A.ld/*k*/, &beta, C.ptr(), C.ld ));
}


void matop::GEMM_StridedBatched(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const float           alpha,
				const matrix_t& A,
				const matrix_t& B,
				const float           beta,
				matrix_t&     C,
				const int batchSize)
{
	int bcols = B.rows;
	int brows = B.cols;
	int acols = A.rows;
	int arows = A.cols;
	if (transb == CUBLAS_OP_T)
	{
		bcols = B.cols;
		brows = B.rows;
	}
	if (transa == CUBLAS_OP_T)
	{
		acols = A.cols;
		arows = A.rows;
	}
	assert(arows == bcols);
	checkCublasErrors(cublasSgemmStridedBatched(cublasHandle, transb, transa,brows,acols,arows, &alpha, B.ptr(), B.ld/*n*/, B.stride, A.ptr(), A.ld/*k*/, A.stride, &beta, C.ptr(), C.ld, C.stride, batchSize ));
}


/*void matop::Reduce2D(const cublasHandle_t& cublasHandle, cublasOperation_t transa,
			const float alpha,
			const matrix_t& A,
			const float beta,
			matrix_t& result)
{
	int cols = 0;
	if (transa == CUBLAS_OP_N)
	{
		transa = CUBLAS_OP_T;
		cols = A.cols;
	}
	else
	{
		transa = CUBLAS_OP_N;
		cols = A.rows;
	}
	thrust::device_vector<float> d_ones(cols, 1.f);
	checkCublasErrors(cublasSgemv(cublasHandle, transa, A.cols, A.rows, &alpha, A.ptr(), A.ld,
		                               thrust::raw_pointer_cast(d_ones.data()), 1, &beta, result.ptr(), 1));
}*/
void matop::Transpose( const cublasHandle_t& cublasHandle, const matrix_t& input, matrix_t& output)
{
	assert(input.cols == output.rows);
	assert(input.rows == output.cols);
	float alpha = 1;
	float beta = 0;
	checkCublasErrors(cublasSgeam(cublasHandle,
		                         CUBLAS_OP_T, CUBLAS_OP_N,
		                         input.rows, input.cols,
		                         &alpha,
		                         input.ptr(), input.ld,
		                         &beta,
		                         output.ptr(), output.ld,
		                         output.ptr(), output.ld));
}
void matop::Transpose( const cublasHandle_t& cublasHandle,
			const int rows, const int cols,
			const thrust::device_ptr<float>& input,
			const thrust::device_ptr<float>& output)
{
	float alpha = 1;
	float beta = 0;
	checkCublasErrors(cublasSgeam(cublasHandle,
	                          CUBLAS_OP_T, CUBLAS_OP_N,
	                          rows, cols,
	                          &alpha,
	                          thrust::raw_pointer_cast(input), cols,
	                          &beta,
	                          thrust::raw_pointer_cast(output), rows,
	                          thrust::raw_pointer_cast(output), rows));
}




