#include "layer.h"
#include "error_util.h"
#include "helper.h"
#include "matop.h"

matop::matrix_t layer_t::s;
matop::matrix_t layer_t::sgrad;
matop::matrix_t layer_t::ds;
matop::matrix_t layer_t::grad;
matop::matrix_t layer_t::grad2;
matop::matrix_t layer_t::stateTensor;

thrust::device_vector<float> layer_t::d_ones;
thrust::device_vector<float> layer_t::s_data;
thrust::device_vector<float> layer_t::sgrad_data;
thrust::device_vector<float> layer_t::ds_data;
thrust::device_vector<float> layer_t::grad_data;
thrust::device_vector<float> layer_t::grad2_data;
thrust::device_vector<float> layer_t::stateTensor_data;

matop::matSequence_t layer_t::hLSTMTemp;
matop::matrix_t layer_t::hLSTMGrad;

thrust::device_vector<float> layer_t::subTensors_data;
thrust::device_vector<float> layer_t::dsubTensors_data;


matop::matrix_t layer_t::subTensors;
matop::matrix_t layer_t::dsubTensors;

std::vector<matop::matrix_t> layer_t::subResults;

std::vector<thrust::device_vector<float>> layer_t::subResults_data;
thrust::device_vector<float> layer_t::hLSTMTemp_data;
thrust::device_vector<float> layer_t::hLSTMGrad_data;

cudaStream_t* layer_t::streams;
layer_t::~layer_t()
{
	//unInit();
}
layer_t::layer_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut , activationType _actTypeHid):
						inputNeurons(_inputNeurons),
						hiddenNeurons(_hiddenNeurons),
						outputNeurons(_outputNeurons),
						numLags(_numLags),
						numOrders(_numOrders),
						activationOutput(_actTypeOut),
						activationHidden(_actTypeHid),
						isInitialized(false)
{   
	batchSize = -1;
	layer_t::InitDataAndMatrix(matop::shape_t {inputNeurons, hiddenNeurons}, wx_data, wx);
	if (outputNeurons > 0)
	{
		layer_t::InitDataAndMatrix(matop::shape_t {hiddenNeurons, outputNeurons}, wy_data, wy);
		layer_t::InitDataAndMatrix(matop::shape_t {outputNeurons, 1}, by_data, by);
		thrust::generate(wy.begin(),wy.end(), helper::xavier_init(hiddenNeurons,std::chrono::system_clock::now().time_since_epoch().count()));
	}

	stateVectorSize = 1+numLags*hiddenNeurons;
	higherOrderStateSize = pow(stateVectorSize,numOrders);

	thrust::generate(wx.begin(),wx.end(), helper::xavier_init(inputNeurons,std::chrono::system_clock::now().time_since_epoch().count()));


}

RNNState layer_t::getLastStates(int seqNum, int amount)
{
	RNNState ret;
	for (int i = 0; i<amount; i++)
	{
		ret.push_back(h[seqNum-i].data());
	}
	return ret;
}
RNNState layer_t::getLastStatesGrad(int seqNum, int amount)
{
	RNNState ret;
	for (int i = 0; i<amount; i++)
	{
		ret.push_back(dh[seqNum-i].data());
	}
	return ret;

}
void layer_t::setInitialState(const RNNState state)
{

	for (int i = 0; i < state.size(); i++)
	{
		checkCudaErrors(cudaMemcpy(h[0-1-i].ptr(), //-1 cause we use it to initialize the new stateTensor
				thrust::raw_pointer_cast(state[i]), h.sizePerElem*sizeof(float),  cudaMemcpyDeviceToDevice/*,  streams[i % streamCount]*/ ));
	}

}
void layer_t::setInitialStateGrad(const RNNState state, int seqNum)
{
	for (int i = 0; i < state.size(); i++)
	{
			checkCudaErrors(cudaMemcpy(dh[seqNum-i].ptr(), //-1 cause we use it to initialize the new stateTensor
					thrust::raw_pointer_cast(state[i]), dh.sizePerElem*sizeof(float),  cudaMemcpyDeviceToDevice/*,  streams[i % streamCount]*/ ));
	}
}

void layer_t::Init(int _batchSize, matop::matSequence_t*& data, matop::matSequence_t*& gradData, bool _isTraining, int _seqLength)
{
	//if (isInitialized) unInit();
	isTraining = _isTraining;
    batchSize = _batchSize;
    if (isTraining)
	{
    	seqLength = _seqLength;
    	InitDataAndMatrix(matop::shape_t{wx.rows, wx.cols}, dwx_data, dwx);

    	InitDataAndMatrix(matop::shape_t{wh.rows, wh.cols}, dwh_data, dwh);

    	InitDataAndMatrix(matop::shape_t{wx.rows, wx.cols}, dwx_sq_data, dwx_sq);

    	InitDataAndMatrix(matop::shape_t{wh.rows, wh.cols}, dwh_sq_data, dwh_sq);
    	
    	InitDataAndMatrix(matop::shape_t{wx.rows, wx.cols}, dwx_sq2_data, dwx_sq2);

    	InitDataAndMatrix(matop::shape_t{wh.rows, wh.cols}, dwh_sq2_data, dwh_sq2);

    	dx = gradData;

    	InitDataAndMatrix(matop::shape_t{batchSize, hiddenNeurons},(seqLength+numLags), dh_data, dh);
    	if (outputNeurons > 0)
    	{

    		InitDataAndMatrix(matop::shape_t{wy.rows, wy.cols}, dwy_data, dwy);

    		InitDataAndMatrix(matop::shape_t{by.rows, by.cols}, dby_data, dby);

    		InitDataAndMatrix(matop::shape_t{wy.rows, wy.cols}, dwy_sq_data, dwy_sq);

    		InitDataAndMatrix(matop::shape_t{by.rows, by.cols}, dby_sq_data, dby_sq);
    		
    		InitDataAndMatrix(matop::shape_t{wy.rows, wy.cols}, dwy_sq2_data, dwy_sq2);

    		InitDataAndMatrix(matop::shape_t{by.rows, by.cols}, dby_sq2_data, dby_sq2);


    		InitDataAndMatrix(matop::shape_t{batchSize, outputNeurons}, seqLength+1, dy_data, dy);

    		gradData = &dy;
    	}
    	else
    	{
    		gradData = &dh;
    	}



	}
    else
    {
    	seqLength = 1;
    }

    x = data;

    ;
    InitDataAndMatrix(matop::shape_t{batchSize, hiddenNeurons},seqLength+numLags, h_data, h);
    if (outputNeurons > 0)
    {
    	InitDataAndMatrix(matop::shape_t{batchSize, outputNeurons}, seqLength+1, y_data, y);
    	data = &y;
    }
    else
    {
    	data = &h;
    }

    seqCounter = 0;
    InitLayerSpecific();
    isInitialized = true;
}
void layer_t::InitLayerSpecific()
{
	stateTarget = &h;
	dstateTarget = &dh;
}

void layer_t::resetStates()
{
	thrust::fill_n(h[seqLength].begin(), h.sizePerElem*numLags,0);
}
void layer_t::resetStateGrad()
{
	thrust::fill_n(dh[0].begin(), dh.size(),0);
}

void layer_t::setInput(matop::matSequence_t* inData, matop::matSequence_t*inDataGrad)
{
	x = inData;
	dx = inDataGrad;
}

void layer_t::CopyBias(const matop::matrix_t& bias, matop::matrix_t& _y, int batch )
{
	for (int i = 0; i<batch; i++)
			thrust::copy(bias.begin(),bias.end(),_y.begin()+i*bias.size());
}
void layer_t::AddBias(const cublasHandle_t& cublasHandle, const matop::matrix_t& bias, matop::matrix_t& _y, int batch )
{
	//thrust::device_vector<float> ones(batch,1);
	float alpha = 1;
	checkCublasErrors(cublasSger(cublasHandle, bias.rows, batch, &alpha, bias.ptr(), 1, d_ones.data().get(), 1, _y.ptr(), _y.ld ));
}
/*void layer_t::ForwardData(	const cublasHandle_t& cublasHandle,
							const int rowsA, const int colsA, const int rowsB,
							const float alpha, const thrust::device_ptr<float>& A,
							const float beta,  const thrust::device_ptr<float>& B,
							const thrust::device_ptr<float>& Result)
{
	float* _A = thrust::raw_pointer_cast(A);
	float* _B = thrust::raw_pointer_cast(B);
	float* res = thrust::raw_pointer_cast(Result);

	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, rowsA, colsA, rowsB, &alpha, _A, _B, &beta, res);
}*/

void layer_t::CreateStateVector(matop::matrix_t& _s, int seqNum)
{
    int firstIdx = seqNum-numLags;
	float* hfirst = h[firstIdx].ptr();
	int numLagsfirst = (firstIdx>=0) ? numLags : -firstIdx;
	float* hend = h[0].ptr();
	
	
	helper::CreateVec(hfirst,hend, hiddenNeurons, numLagsfirst, numLags, batchSize, _s.ptr());
}
//suppose s is without the bias 1
void layer_t::ScatterStateVector(const cublasHandle_t& cublasHandle, matop::matrix_t& _ds, int seqNum)
{

	for (int i = 0; i<numLags; i++)
	{
		matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, batchSize, hiddenNeurons, 1.f,
				_ds.data()+1+i*hiddenNeurons, _ds.ld,
				dh[seqNum-i].data(), dh[seqNum-i].ld,
				1.f,
				dh[seqNum-i].data(), dh[seqNum-i].ld);

		/*matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
				batchSize, hiddenNeurons,
				1.f,
				_ds.data()+i*hiddenNeurons+1, _ds.ld,
				dh[seqNum-i].data(), dh[seqNum-i].ld,
				1.f,
				dh[seqNum-i].data(), dh[seqNum-i].ld);*/

	}

}

//cublasHandle is needed for the LSTM function
void layer_t::ActivationState(const cublasHandle_t& cublasHandle, matop::matrix_t& input, matop::matrix_t& output)
{
	Activation(input.data(), input.size(), output.data(), activationHidden);
}

void layer_t::ForwardInference(const cublasHandle_t& cublasHandle)
{
	matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
			1.f,
			(*x)[seqCounter],
			wx,
			0.f,
			(*stateTarget)[seqCounter]);

	CreateStateVector(s, seqCounter);
	ForwardState(cublasHandle, s, (*stateTarget)[seqCounter]);

	ActivationState(cublasHandle,(*stateTarget)[seqCounter],h[seqCounter]);
	if (outputNeurons > 0)
	{
		//this part is only relevant for decoder out

		matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
				1.f,
				h[seqCounter],
				wy,
				0.f,
				y[seqCounter+1]);
		AddBias(cublasHandle, by, y[seqCounter+1], batchSize);
		Activation(y[seqCounter+1].data(), y.sizePerElem, y[seqCounter+1].data(), activationOutput );
	}
}

void layer_t::ForwardTraining(const cublasHandle_t& cublasHandle, int seqNum)
{
	seqCounter = seqNum;
	ForwardInference(cublasHandle);
}

//cublasHandle is needed for the LSTM function
void layer_t::ActivationStateGrad(const cublasHandle_t& cublasHandle, const matop::matrix_t& inputGrad, matop::matrix_t& outputGrad)
{
	ActivationGradient(h[seqCounter].data(),outputGrad.data(), h.sizePerElem, activationHidden);
}

void layer_t::BackwardTraining(const cublasHandle_t& cublasHandle)
{
	seqCounter++; // cause seqCounter doesnt init in the first round
	for (int i = 0; i < seqLength; i++)
	{
		seqCounter--;

		if (outputNeurons > 0)
		{
			ActivationGradient(y[seqCounter+1].data(),dy[seqCounter+1].data(), y.sizePerElem, activationOutput);

			layer_t::Reduce2D(cublasHandle, CUBLAS_OP_T,
					1,
					dy[seqCounter+1],
					1,
					dby);
			matop::GEMM(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
					1,
					h[seqCounter],
					dy[seqCounter+1],
					1,
					dwy);
			matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
					1,
					dy[seqCounter+1],
					wy,
					1,
					dh[seqCounter]);
		}

		ActivationStateGrad(cublasHandle, dh[seqCounter], (*dstateTarget)[seqCounter]);

		matop::GEMM(cublasHandle, CUBLAS_OP_T, CUBLAS_OP_N,
				1,
				(*x)[seqCounter],
				(*dstateTarget)[seqCounter],
				1,
				dwx);

		if (dx != nullptr) //the first gradient data does not need to be calculated
		{
		matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
				1,
				(*dstateTarget)[seqCounter],
				wx,
				1,
				(*dx)[seqCounter]);
		}
		CreateStateVector(s, seqCounter);
		BackwardState(cublasHandle, s, (*dstateTarget)[seqCounter], sgrad);
		ScatterStateVector(cublasHandle, sgrad,seqCounter-1);

	}

}

void layer_t::UpdateWeights(float learningRate, int stepNum)
{

    ADAM(learningRate, stepNum);
	//RMSProp(learningRate);
	//SGD(learningRate, 0.5);
}
void layer_t::ADAM(float learningRate, int stepNum)
{
float moment1 = 0.9;
float moment2 = 0.999;
if (outputNeurons > 0)
{
    thrust::transform( thrust::make_zip_iterator(thrust::make_tuple(dwy.begin(), dwy_sq.begin(), dwy_sq2.begin(),wy.begin())),
                    thrust::make_zip_iterator(thrust::make_tuple(dwy.end(), dwy_sq.end(), dwy_sq2.end(),wy.end())),
                        thrust::make_zip_iterator(thrust::make_tuple(dwy.begin(), dwy_sq.begin(), dwy_sq2.begin(),wy.begin())),
                        helper::Adam(moment1, moment2, stepNum,learningRate));
    thrust::transform( thrust::make_zip_iterator(thrust::make_tuple(dby.begin(), dby_sq.begin(), dby_sq2.begin(),by.begin())),
                    thrust::make_zip_iterator(thrust::make_tuple(dby.end(), dby_sq.end(), dby_sq2.end(),by.end())),
                        thrust::make_zip_iterator(thrust::make_tuple(dby.begin(), dby_sq.begin(), dby_sq2.begin(),by.begin())),
                        helper::Adam(moment1, moment2, stepNum,learningRate));
     
                        }
    /* std::cout<< dwh_data[0] << std::endl;
     std::cout<< dwh_sq_data[0] << std::endl;
     std::cout<< dwh_sq2_data[0] << std::endl;
     std::cout<< wh_data[0] << std::endl;*/
    thrust::transform( thrust::make_zip_iterator(thrust::make_tuple(dwh.begin(), dwh_sq.begin(), dwh_sq2.begin(),wh.begin())),
                    thrust::make_zip_iterator(thrust::make_tuple(dwh.end(), dwh_sq.end(), dwh_sq2.end(),wh.end())),
                        thrust::make_zip_iterator(thrust::make_tuple(dwh.begin(), dwh_sq.begin(), dwh_sq2.begin(),wh.begin())),
                        helper::Adam(moment1, moment2, stepNum,learningRate));
    /*                        std::cout<< dwh_data[0] << std::endl;
     std::cout<< dwh_sq_data[0] << std::endl;
     std::cout<< dwh_sq2_data[0] << std::endl;
     std::cout<< wh_data[0] << std::endl;*/
                       
    thrust::transform( thrust::make_zip_iterator(thrust::make_tuple(dwx.begin(), dwx_sq.begin(), dwx_sq2.begin(),wx.begin())),
                    thrust::make_zip_iterator(thrust::make_tuple(dwx.end(), dwx_sq.end(), dwx_sq2.end(),wx.end())),
                        thrust::make_zip_iterator(thrust::make_tuple(dwx.begin(), dwx_sq.begin(), dwx_sq2.begin(),wx.begin())),
                        helper::Adam(moment1, moment2, stepNum,learningRate));
                      
                        
                        
    
}
void layer_t::RMSProp(float learningRate)
{
	float alpha = -learningRate/1.f;
	float beta = 0.9;
	if (outputNeurons > 0)
	{
		thrust::transform(dwy_sq.begin(), dwy_sq.end(), dwy.begin(), dwy_sq.begin(), helper::movingAverageSq(1.f,beta));
		thrust::transform(	thrust::make_zip_iterator(thrust::make_tuple(wy.begin(), 	dwy_sq.begin(), dwy.begin())),
							thrust::make_zip_iterator(thrust::make_tuple(wy.end(),		dwy_sq.end(), 	dwy.end())),
							wy.begin(),  helper::RMSPropUpdate(alpha));
		thrust::fill(dwy.begin(), dwy.end(), 0);
		thrust::transform(dby_sq.begin(), dby_sq.end(), dby.begin(), dby_sq.begin(), helper::movingAverageSq(1.f,beta));
		thrust::transform(	thrust::make_zip_iterator(thrust::make_tuple(by.begin(), dby_sq.begin(), dby.begin())),
							thrust::make_zip_iterator(thrust::make_tuple(by.end(), dby_sq.end(), dby.end())),
							by.begin(),  helper::RMSPropUpdate(alpha));
		thrust::fill(dby.begin(), dby.end(), 0);
	}
	thrust::transform(dwx_sq.begin(), dwx_sq.end(), dwx.begin(), dwx_sq.begin(), helper::movingAverageSq(1.f,beta));

	thrust::transform(	thrust::make_zip_iterator(thrust::make_tuple(wx.begin(), dwx_sq.begin(), dwx.begin())),
						thrust::make_zip_iterator(thrust::make_tuple(wx.end(), dwx_sq.end(), dwx.end())),
						wx.begin(),  helper::RMSPropUpdate(alpha));

	thrust::fill(dwx.begin(), dwx.end(), 0);
	thrust::transform(dwh_sq.begin(), dwh_sq.end(), dwh.begin(), dwh_sq.begin(), helper::movingAverageSq(1.f,beta));
	thrust::transform(	thrust::make_zip_iterator(thrust::make_tuple(wh.begin(), dwh_sq.begin(), dwh.begin())),
						thrust::make_zip_iterator(thrust::make_tuple(wh.end(), dwh_sq.end(), dwh.end())),
						wh.begin(),  helper::RMSPropUpdate(alpha));
	thrust::fill(dwh.begin(), dwh.end(), 0);

}
void layer_t::SGD(float learningRate, float momentum)
{
	float alpha = -learningRate;
	float scale = batchSize*(seqCounter+seqLength); //already one sequence back when update
	if (outputNeurons > 0)
	{
		thrust::transform(dwy_sq.begin(), dwy_sq.end(), dwy.begin(), dwy_sq.begin(), helper::saxpy_functor(momentum, (1-momentum)/scale));
		thrust::transform(wy.begin(), wy.end(), dwy_sq.begin(), wy.begin(), helper::saxpy_functor(1.f, alpha));
		thrust::fill(dwy.begin(), dwy.end(), 0);
		thrust::transform(dby_sq.begin(), dby_sq.end(), dby.begin(), dby_sq.begin(), helper::saxpy_functor(momentum, (1-momentum)/scale));
		thrust::transform(by.begin(), by.end(), dby_sq.begin(), by.begin(), helper::saxpy_functor(1.f, alpha));
		thrust::fill(dby.begin(), dby.end(), 0);
	}
	thrust::transform(dwx_sq.begin(), dwx_sq.end(), dwx.begin(), dwx_sq.begin(), helper::saxpy_functor(momentum, (1-momentum)/scale));
	thrust::transform(wx.begin(), wx.end(), dwx_sq.begin(), wx.begin(), helper::saxpy_functor(1.f, alpha));
	thrust::fill(dwx.begin(), dwx.end(), 0);
	thrust::transform(dwh_sq.begin(), dwh_sq.end(), dwh.begin(), dwh_sq.begin(), helper::saxpy_functor(momentum, (1-momentum)/scale));
	thrust::transform(wh.begin(), wh.end(), dwh_sq.begin(), wh.begin(), helper::saxpy_functor(1.f, alpha));
	thrust::fill(dwh.begin(), dwh.end(), 0);

}

void layer_t::Activation(const thrust::device_ptr<float> start, size_t elements, thrust::device_ptr<float> result, activationType _actType )
{

	if (_actType == Sigmoid)
		thrust::transform(thrust::device, start, start+elements, result, helper::sigmoid_f());
	else if (_actType == TanH)
		thrust::transform(thrust::device, start, start+elements, result, helper::tanh_f());
}

void layer_t::ActivationGradient(const thrust::device_ptr<float> hiddenState, const thrust::device_ptr<float> hiddenStateGrad, int size, activationType act )
{
	if (act == Sigmoid)
		thrust::transform(thrust::device, hiddenState, hiddenState+size,hiddenStateGrad, hiddenStateGrad/*+hiddenState.size()*/, helper::sigmoid_fg());
	else if (act == TanH)
		thrust::transform(thrust::device, hiddenState, hiddenState+size,hiddenStateGrad, hiddenStateGrad/*+hiddenState.size()*/, helper::tanh_fg());

}


void layer_t::InitDataAndMatrix(const matop::shape_t& shape, thrust::device_vector<float>& data, matop::matrix_t& mat)
{
	assert(shape.size()==2);
	data = thrust::device_vector<float>(shape[0]*shape[1]);
	mat = matop::matrix_t(data.data(), shape[0],shape[1]);
}
/*void layer_t::InitDataAndTensor(const matop::shape_t& shape, const int seqLength, thrust::device_vector<float>& data, matop::sequence_t<float>& tens)
{
	data = thrust::device_vector<float>(seqLength*std::accumulate(shape.begin(), shape.end(), 1, std::multiplies<int>()));
	tens = matop::sequence_t<float>(data.data(), shape, seqLength);
}*/
void layer_t::InitDataAndMatrix(const matop::shape_t& shape, const int seqLength, thrust::device_vector<float>& data, matop::matSequence_t& tens)
{
	data = thrust::device_vector<float>(seqLength*std::accumulate(shape.begin(), shape.end(), 1, std::multiplies<int>()));
	tens = matop::matSequence_t(data.data(), shape, seqLength);
}


void layer_t::initStaticHelpers(int numLags, int hiddenNeurons, int batchSize, int numOrders, bool RNN, bool RNNTT, bool LSTMTT, std::vector<int> TT_ranks)
{
	int sizeOfOneStateVector = (numLags*hiddenNeurons+1);
	int higherOrderStateSize = pow(sizeOfOneStateVector,numOrders);
	matop::shape_t tensorshape{batchSize, higherOrderStateSize};
	//just ones for the reduce and add bias op
	d_ones = thrust::device_vector<float>(batchSize,1);

	InitDataAndMatrix(matop::shape_t{batchSize, sizeOfOneStateVector}, s_data, s);
	InitDataAndMatrix(matop::shape_t{batchSize, sizeOfOneStateVector}, sgrad_data, sgrad);
	helper::strided_range<thrust::device_vector<float>::iterator> strided_s(s.begin(), s.end(), sizeOfOneStateVector);
			thrust::fill(strided_s.begin(), strided_s.end(),1);
	if (RNN)
	{
		InitDataAndMatrix(matop::shape_t{sizeOfOneStateVector,sizeOfOneStateVector}, ds_data, ds);
		InitDataAndMatrix(tensorshape, stateTensor_data, stateTensor);
		//tensorshape.push_back(sizeOfOneStateVector);
		InitDataAndMatrix(matop::shape_t{batchSize,sizeOfOneStateVector*higherOrderStateSize}, grad_data, grad);
		InitDataAndMatrix(matop::shape_t{batchSize,sizeOfOneStateVector*higherOrderStateSize}, grad2_data, grad2);

		//create identity matrix
		helper::strided_range<thrust::device_vector<float>::iterator> strided_ds(ds.begin(), ds.end(), sizeOfOneStateVector+1);
		thrust::fill(strided_ds.begin(), strided_ds.end(), 1);
	}
	if (RNNTT ||LSTMTT)
	{
		assert(TT_ranks.size() == numOrders-1);

		subResults = std::vector<matop::matrix_t>();
		subResults_data = std::vector<thrust::device_vector<float>>();
		int numUnits = (RNNTT) ? 1 : 4;
		TT_ranks.push_back(hiddenNeurons*numUnits);
		TT_ranks.insert(TT_ranks.begin(), 1);

		int colSize = 0;
		for (int i = 0; i< numOrders; i++)
		{
			colSize += TT_ranks[i]*TT_ranks[i+1];
		}
		InitDataAndMatrix(matop::shape_t{batchSize,colSize}, subTensors_data, subTensors);
		InitDataAndMatrix(matop::shape_t{batchSize,colSize}, dsubTensors_data, dsubTensors);

		for (int i = 0; i< numOrders-2; i++)
		{
			thrust::device_vector<float> data;
			matop::matrix_t tensor;
			InitDataAndMatrix(matop::shape_t{batchSize,TT_ranks[0]*TT_ranks[i+2]}, data, tensor);
			subResults.push_back(tensor);
			subResults_data.push_back(data);
		}

	}
	if (LSTMTT)
	{
		InitDataAndMatrix(matop::shape_t{batchSize,hiddenNeurons*4},1, hLSTMTemp_data, hLSTMTemp);
		InitDataAndMatrix(matop::shape_t{4,batchSize*hiddenNeurons}, hLSTMGrad_data, hLSTMGrad);
	}

	/*if (streams != nullptr)
		delete streams;
	streams = new cudaStream_t[streamCount];
	for (int i =0 ; i<streamCount; i++)
	{
		checkCudaErrors(cudaStreamCreate(&streams[i]));
	}*/

}
//needed to explicity destroy the static variables, or there will be an error on exit
void layer_t::destroyHelper()
{
	s= matop::matrix_t();
	s_data= thrust::device_vector<float>();

	sgrad = matop::matrix_t();
	sgrad_data= thrust::device_vector<float>();

	ds = matop::matrix_t();
	ds_data= thrust::device_vector<float>();

	grad = matop::matrix_t();
	grad_data= thrust::device_vector<float>();

	grad2 = matop::matrix_t();
	grad2_data= thrust::device_vector<float>();

	stateTensor = matop::matrix_t();
	stateTensor_data= thrust::device_vector<float>();

	subTensors = matop::matrix_t();
	subTensors_data= thrust::device_vector<float>();

	dsubTensors = matop::matrix_t();
	dsubTensors_data= thrust::device_vector<float>();

	subResults = std::vector<matop::matrix_t>();
	subResults_data= std::vector<thrust::device_vector<float>>();

	hLSTMTemp = matop::matSequence_t();
	hLSTMTemp_data= thrust::device_vector<float>();

	hLSTMGrad = matop::matrix_t();
	hLSTMGrad_data= thrust::device_vector<float>();

	d_ones = thrust::device_vector<float>();

}



void layer_t::SetWeights(matop::matrix_t _Wx, matop::matrix_t _Wy, matop::matrix_t _Wh, matop::matrix_t _by )
{
	assert(_Wx.size() == wx.size());
	//assert(_Wy.size() == wy.size());
	assert(_Wh.size() == wh.size());
	//assert(_by.size() == by.size());
	if (_Wy.size() != wy.size() || _by.size() != by.size())
	{
		std::cout << "Warning: by and wy are not set because of non-matching size" << std::endl;
	}
	else
	{
		thrust::copy(_Wy.begin(), _Wy.end(), wy.begin());
		thrust::copy(_by.begin(), _by.end(), by.begin());
	}
	thrust::copy(_Wx.begin(), _Wx.end(), wx.begin());
	thrust::copy(_Wh.begin(), _Wh.end(), wh.begin());
}
void layer_t::GetWeightGrad(matop::matrix_t& _Wx, matop::matrix_t& _Wy, matop::matrix_t& _Wh )
{
	_Wx = dwx;
	_Wy = dwy;
	_Wh = dwh;
}

void layer_t::Reduce2D(const cublasHandle_t& cublasHandle, cublasOperation_t transa,
			const float alpha,
			const matop::matrix_t& A,
			const float beta,
			matop::matrix_t& result)
{
	int cols = 0;
	if (transa == CUBLAS_OP_N)
	{
		transa = CUBLAS_OP_T;
		cols = A.cols;
	}
	else
	{
		transa = CUBLAS_OP_N;
		cols = A.rows;
	}
	//d_ones(cols, 1.f);
	checkCublasErrors(cublasSgemv(cublasHandle, transa, A.cols, A.rows, &alpha, A.ptr(), A.ld,
		                               thrust::raw_pointer_cast(d_ones.data()), 1, &beta, result.ptr(), 1));

}
