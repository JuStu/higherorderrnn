/*
 * TTLSTM.cu


 *
 *  Created on: Jun 20, 2020
 *      Author: julian
 */
#include "TTLSTM.h"
#include "helper.h"
void LSTMTT_t::resetStates()
{
	thrust::fill_n(h[seqLength].data(), h.sizePerElem*numLags,0); //reset initial h, (sequence ends with the newest states
	thrust::fill_n(c[-1].data(), c.sizePerElem, 0); //reset initial c
}
void LSTMTT_t::resetStateGrad()
{
	thrust::fill_n(dh.begin(), dh.size(),0);
	thrust::fill(dc.begin(), dc.end(),0);
}
MemState LSTMTT_t::getLastMemory(int seqNum)
{
	return c[seqNum].data();
}
MemState LSTMTT_t::getLastMemoryGrad(int seqNum) //seqNum can be delteed
{
	return dc.data();
}
 void LSTMTT_t::setInitialMemory(const MemState state)
{
	 thrust::copy_n(state,c.sizePerElem,c[-1].begin());
}
 void LSTMTT_t::setInitialMemoryGrad(const MemState state)
 {
	 thrust::copy_n(state, dc.size(), dc.begin());
 }
LSTMTT_t::LSTMTT_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut, activationType _actTypeHid, std::vector<int> _TT_ranks):
		RNNTT_t(_inputNeurons, _hiddenNeurons, _outputNeurons,  _numLags, _numOrders, _actTypeOut , _actTypeHid, _TT_ranks), numUnits(4)
{
	//numUnits = [input, forget, output, new_input]
	//override wx
	layer_t::InitDataAndMatrix(matop::shape_t{inputNeurons,hiddenNeurons*numUnits}, wx_data, wx);
	thrust::generate(wx.begin(),wx.end(), helper::xavier_init(inputNeurons,std::chrono::system_clock::now().time_since_epoch().count()));
	TT_ranks = _TT_ranks; //override TT_ranks from RNNTT constructor
	TT_ranks.insert(TT_ranks.begin(), 1);
	assert(TT_ranks.size() == numOrders);
	TT_ranks.push_back(numUnits*hiddenNeurons);
	TTRanksStacked = 0;
	for (int i = 0; i< TT_ranks.size()-1; ++i)
	{
		TTRanksStacked += TT_ranks[i]*TT_ranks[i+1];
	}
	layer_t::InitDataAndMatrix(matop::shape_t{stateVectorSize, TTRanksStacked}, wh_data, wh);
	int startStride = 0;
	for (int i = 0; i<numOrders; i++)
	{
		for (int j = 0; j < stateVectorSize; j++) //memory layout is (stateVectorSize, TTRanksStacked) with each kernel of size (stateVectorSize, R_i, R_(i+1))
		{
			thrust::generate(wh.begin()+ j*TTRanksStacked + startStride,wh.begin()+ + j*TTRanksStacked + startStride + TT_ranks[i]*TT_ranks[i+1],
					helper::glorot_init(stateVectorSize, TT_ranks[i], TT_ranks[i+1],std::chrono::system_clock::now().time_since_epoch().count()));
		}
		startStride += TT_ranks[i]*TT_ranks[i+1];
	}
}
void LSTMTT_t::InitLayerSpecific()
{
	RNNTT_t::InitLayerSpecific();


	stateTarget = &hLSTMTemp;
	dstateTarget = &hLSTMTemp;
	layer_t::InitDataAndMatrix(matop::shape_t{numUnits,batchSize*hiddenNeurons}, seqLength, hLSTM_data, hLSTM);

	layer_t::InitDataAndMatrix(matop::shape_t{batchSize,hiddenNeurons}, seqLength+1, c_data, c);

	if (isTraining)
	{
		//dhLSTM = matop::sequence_t<float>(numUnits*batchSize*hiddenNeurons,seqLength );;
		layer_t::InitDataAndMatrix(matop::shape_t{batchSize,hiddenNeurons}, dc_data, dc);
	}
}
/*void LSTMTT_t::ForwardInference(const cublasHandle_t& cublasHandle)
{

	ForwardData(	cublasHandle,
					batchSize, inputNeurons, hiddenNeurons*numUnits,
					1.0, (*x)[seqCounter],
					0.0, &wx[0],
					&hLSTMTemp[0]);

	CreateStateVector(s, seqCounter);
	ForwardState(	cublasHandle,&hLSTMTemp[0]); //hLSTM is a array of batchSize*hiddenSize*numUnits, with numUnits = [input, new_input, forget, output] gate


	LSTMStepForward(cublasHandle);

	if (outputNeurons > 0)
	{
		//this part is only relevant for decoder out
		CopyBias(by, y[seqCounter+1], batchSize);
		ForwardData(	cublasHandle,
						batchSize, hiddenNeurons, outputNeurons,
						1.0, h[seqCounter],
						1.0, &wy[0],
						y[seqCounter+1]);
		Activation(y[seqCounter+1], y.sizePerElem, y[seqCounter+1], activationOutput );
	}
}
void LSTMTT_t::BackwardTraining(const cublasHandle_t& cublasHandle)
{
	seqCounter++; // cause seqCounter doesnt init in the first round
	for (int i = 0; i < seqLength; i++)
	{
		seqCounter--;

		if (outputNeurons > 0)
		{
			ActivationGradient(y[seqCounter+1],dy[seqCounter+1], y.sizePerElem, activationOutput);
			matop::Reduce2D(cublasHandle,
					batchSize, outputNeurons,
					1, dy[seqCounter+1],
					1, false,
					&dby[0]
					);

			BackwardWeights(cublasHandle,
					batchSize, hiddenNeurons, outputNeurons,
					1,h[seqCounter],
					1,dy[seqCounter+1],
					&dwy[0]);
			BackwardData(cublasHandle,
					batchSize, hiddenNeurons, outputNeurons,
					1, dy[seqCounter+1],
					1, &wy[0],
					dh[seqCounter]);
		}


		LSTMStepBackward(cublasHandle);


		BackwardWeights(cublasHandle,
					batchSize, inputNeurons, hiddenNeurons*numUnits,
					1,(*x)[seqCounter],
					1,&hLSTMGrad[0],
					&dwx[0]);

		if (dx != nullptr) //the first gradient data does not need to be calculated
		{
		BackwardData(cublasHandle,
					batchSize, inputNeurons, hiddenNeurons*numUnits,
					1, &hLSTMGrad[0],
					1, &wx[0],
					(*dx)[seqCounter]);
		}
		CreateStateVector(s, seqCounter);

		BackwardState(cublasHandle, &hLSTMGrad[0]);
		ScatterStateVector(cublasHandle, sgrad,seqCounter-1);
	}

}*/
void LSTMTT_t::ActivationState(const cublasHandle_t& cublasHandle,  matop::matrix_t& input, matop::matrix_t& output)
{
							//numUnits*batchSize*hiddenSize
	matop::Transpose(cublasHandle,
					batchSize*hiddenNeurons, numUnits,
					input.data(),
					hLSTM[seqCounter].data()); //transpose for easier split

	const int ld = hLSTM[seqCounter].ld;
	thrust::device_ptr<float> j = hLSTM[seqCounter].data()+3*ld;//new input gate
	thrust::device_ptr<float> i = hLSTM[seqCounter].data();
	thrust::device_ptr<float> f = hLSTM[seqCounter].data()+ld;
	thrust::device_ptr<float> o = hLSTM[seqCounter].data()+2*ld;
	thrust::device_ptr<float> c_old = c[seqCounter-1].data();

	Activation(hLSTM[seqCounter].data(),3*ld, hLSTM[seqCounter].data(), Sigmoid);
	Activation(hLSTM[seqCounter].data()+3*ld, hLSTM[seqCounter].ld, hLSTM[seqCounter].data()+3*hLSTM[seqCounter].ld, TanH);
	//numUnits = [input, forget, output, new_input] gate
	thrust::transform(thrust::make_zip_iterator(thrust::make_tuple(c_old,f, i, j))
						,thrust::make_zip_iterator(thrust::make_tuple(c_old+ld,f+ld, i+ld, j+ld)),
						c[seqCounter].data(), helper::c_update());
	thrust::transform(c[seqCounter].begin(), c[seqCounter].end(),o,h[seqCounter].begin(),helper::h_update());
}
//input grad is dh and outputGrad is hLSTMTemp
void LSTMTT_t::ActivationStateGrad(const cublasHandle_t& cublasHandle, const matop::matrix_t& inputGrad, matop::matrix_t& outputGrad)
{

	const int ld = hLSTMGrad.ld;
	thrust::device_ptr<float> d_j = hLSTMGrad.data()+3*ld;//new input gate
	thrust::device_ptr<float> d_i = hLSTMGrad.data();
	thrust::device_ptr<float> d_f = hLSTMGrad.data()+ld;
	thrust::device_ptr<float> d_o = hLSTMGrad.data()+2*ld;

	thrust::device_ptr<float> j = hLSTM[seqCounter].data()+3*ld;//new input gate
	thrust::device_ptr<float> i = hLSTM[seqCounter].data();
	thrust::device_ptr<float> f = hLSTM[seqCounter].data()+ld;
	thrust::device_ptr<float> o = hLSTM[seqCounter].data()+2*ld;
	thrust::device_ptr<float> c_old = c[seqCounter-1].data();


	thrust::transform(c[seqCounter].begin(), c[seqCounter].end(),dh[seqCounter].begin(),d_o,helper::h_update());


	thrust::transform(thrust::make_zip_iterator(thrust::make_tuple(o, dh[seqCounter].begin(), c[seqCounter].begin(), dc.begin()))
					,thrust::make_zip_iterator(thrust::make_tuple(o+ld,dh[seqCounter].end(),c[seqCounter].end(), dc.end())),
							dc.begin(), helper::c_grad());

	thrust::transform(c[seqCounter-1].begin(), c[seqCounter-1].end(), dc.begin(),  d_f, thrust::multiplies<float>());
	thrust::transform(j, j+ld, dc.begin(),  d_i, thrust::multiplies<float>());
	thrust::transform(i, i+ld, dc.begin(),  d_j, thrust::multiplies<float>());

	ActivationGradient(i,d_i, 3*ld, Sigmoid);
	ActivationGradient(j,d_j, ld, TanH);

	thrust::transform(dc.begin(), dc.end(),  f, dc.begin(), thrust::multiplies<float>());

	matop::Transpose(cublasHandle, numUnits, batchSize*hiddenNeurons, hLSTMGrad.data(), outputGrad.data());
}



