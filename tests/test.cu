#include "data_handler.h"
#include "network.h"
#include <assert.h>
#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <set>
#include "error_util.h"
#include <thrust/device_vector.h>
#include <thrust/fill.h>
#include <chrono>
#include <thrust/logical.h>
#include "helper.h"
#include "matop.h"
#include "TTLSTM.h"
#include "TTRNN.h"
#include "RNN.h"
#include <utility>

template class std::vector<char>; //so I can use it in debugger
template class std::vector<unsigned int>; //so I can use it in debugger
template class std::vector<std::vector<char>>;
template class std::vector<thrust::device_vector<float>>;
class networkTestLSTM: public network
{
public:
	using network::network;
	void ForwardBackWard()
		{
			assert(net_type == TTLSTM);
			assert(numLags == 2);
			assert(numOrders == 3);
			assert(hiddenSize == 2);
			assert(numLayers == 2);
			int batchSize = 2;
			std::vector<int> _TT_ranks = {3,3};
			assert(TT_ranks == _TT_ranks);

			assert(inputSize == 2);
			assert(outputSize == 2);

			int _inpSteps = 2;
			int _outSteps = 2; //totalSequenceLength - inpSteps;

			thrust::device_vector<float> data_vec;
			matop::matSequence_t data;
			layer_t::InitDataAndMatrix(matop::shape_t{batchSize,inputSize}, _inpSteps, data_vec, data);
			matop::matSequence_t* x = (&data);
			thrust::device_vector<float> inInit = helper::linspace(-1,1, batchSize*inputSize* _inpSteps);
			thrust::copy(inInit.begin(), inInit.end(), data.begin());

			this->Init(x, batchSize, true, _inpSteps, _outSteps);


			thrust::device_vector<float>Wh_data;
			matop::matrix_t Wh;
			layer_t::InitDataAndMatrix(matop::shape_t{5,3 +3*3+3*8}, Wh_data, Wh);
			matop::matrix_t Wh_m = Wh;

			thrust::device_vector<float> wInit = helper::linspace(-1, 1, 15);

			thrust::copy(wInit.begin(), wInit.begin()+3, Wh.begin());
			thrust::copy(wInit.begin()+3, wInit.begin()+6, Wh.begin()+Wh_m.ld);
			thrust::copy(wInit.begin()+6, wInit.begin()+9, Wh.begin()+Wh_m.ld*2);
			thrust::copy(wInit.begin()+9, wInit.begin()+12, Wh.begin()+Wh_m.ld*3);
			thrust::copy(wInit.begin()+12, wInit.begin()+15, Wh.begin()+Wh_m.ld*4);
			wInit = helper::linspace(0, 1, 45);
			thrust::copy(wInit.begin(), wInit.begin()+9, Wh.begin()+3);
			thrust::copy(wInit.begin()+9, wInit.begin()+18, Wh.begin()+3+Wh_m.ld);
			thrust::copy(wInit.begin()+18, wInit.begin()+27, Wh.begin()+3+Wh_m.ld*2);
			thrust::copy(wInit.begin()+27, wInit.begin()+36, Wh.begin()+3+Wh_m.ld*3);
			thrust::copy(wInit.begin()+36, wInit.begin()+45, Wh.begin()+3+Wh_m.ld*4);
			wInit = helper::linspace(0, 1, 120);
			thrust::copy(wInit.begin(), wInit.begin()+24, Wh.begin()+12);
			thrust::copy(wInit.begin()+24, wInit.begin()+48, Wh.begin()+12+Wh_m.ld);
			thrust::copy(wInit.begin()+48, wInit.begin()+72, Wh.begin()+12+Wh_m.ld*2);
			thrust::copy(wInit.begin()+72, wInit.begin()+96, Wh.begin()+12+Wh_m.ld*3);
			thrust::copy(wInit.begin()+96, wInit.begin()+120, Wh.begin()+12+Wh_m.ld*4);


			thrust::device_vector<float> Wx_data = helper::linspace(-1, 1 , 2*2*4);
			thrust::device_vector<float> Wy_data = helper::linspace(-1, 1 , 2*2);
			thrust::device_vector<float> by_data(outputSize,0);

			matop::matrix_t Wx(Wx_data.data(), 2,8);
			matop::matrix_t Wy(Wy_data.data(), 2,2);
			matop::matrix_t by(by_data.data(), 2,1);

			encoder.SetWeights(Wx,Wy,Wh,by);
			decoder.SetWeights(Wx,Wy,Wh,by);

			this->Forward();
			float result[] = {0.00645287, -0.01841852,
					  0.00682051, -0.03258369};/*{0.02556877, -0.03160132,
			  0.00600675, -0.02204339};*/
			/*{*/
			thrust::device_vector<float> finalOutput(result, result+4);
			thrust::transform(finalOutput.begin(), finalOutput.end(), (*outData)[2].begin(), finalOutput.begin(), helper::abs_diff());
			assert(1e-4 > thrust::reduce(finalOutput.begin(), finalOutput.end()));

			thrust::copy(inInit.begin(), inInit.end(), (*loss)[1].begin());

			this->Backward();
			//std::vector<std::vector<thrust::device_ptr<float>>> statesGrad = encoder.getLastStateGrad(0,2);
			//thrust::copy_n(statesGrad[0][0],4, finalOutput.begin());
			encoder.GetWeightGrads( Wx,  Wy,  Wh , 0);

			/*float dwxres[] = {-6.20166319,  7.9824626,
			 -9.00715791 ,9.15324942};
			thrust::copy(dwxres, dwxres+4, finalOutput.begin());
			thrust::transform(finalOutput.begin(), finalOutput.end(), Wx.begin(), finalOutput.begin(), helper::abs_diff());*/
			assert(1e-5 > thrust::reduce(finalOutput.begin(), finalOutput.end()));


		}
};
class networkTest: public network
{
public:
	using network::network;
	void ForwardBackWard()
		{
			assert(net_type == RNN);
			assert(numLags == 2);
			assert(numOrders == 2);
			assert(hiddenSize == 2);
			assert(numLayers == 2);
			int batchSize = 2;

			assert(inputSize == 2);
			assert(outputSize == 2);

			int _inpSteps = 2;
			int _outSteps = 2; //totalSequenceLength - inpSteps;

			thrust::device_vector<float> data_vec;
			matop::matSequence_t data;
			layer_t::InitDataAndMatrix(matop::shape_t{batchSize, inputSize},2, data_vec, data);
			matop::matSequence_t* x = (&data);
			thrust::device_vector<float> inInit = helper::linspace(-1,1, batchSize*inputSize* _inpSteps);
			thrust::copy(inInit.begin(), inInit.end(), data.begin());

			this->Init(x, batchSize, true, _inpSteps, _outSteps);


			thrust::device_vector<float> Wx_data = helper::linspace(-1, 1 , 4);
			thrust::device_vector<float> Wh_data = helper::linspace(-1, 1 , pow(1+numLags*hiddenSize,2)*hiddenSize);
			thrust::device_vector<float> Wy_data = Wx_data;
			thrust::device_vector<float> by_data(outputSize,0);

			matop::shape_t tensorshape(2, 1+numLags*hiddenSize);
			tensorshape.push_back(2);
			matop::matrix_t Wx(Wx_data.data(), 2,2);
			matop::matrix_t Wy(Wy_data.data(), 2,2);
			matop::matrix_t Wh(Wh_data.data(), pow(1+numLags*hiddenSize,2),2);
			matop::matrix_t by(by_data.data(),2,1);

			encoder.SetWeights(Wx,Wy,Wh,by);
			decoder.SetWeights(Wx,Wy,Wh,by);

			this->Forward();
			float result[] = {-1.2659703,  -1.7719891,
					  27.213285  ,-33.847885 };
			thrust::device_vector<float> finalOutput(result, result+4);
			thrust::transform(finalOutput.begin(), finalOutput.end(), (*outData)[2].begin(), finalOutput.begin(), helper::abs_diff());
			float abs = thrust::reduce(finalOutput.begin(), finalOutput.end());
			assert(1e-3 > thrust::reduce(finalOutput.begin(), finalOutput.end()));

			thrust::copy(inInit.begin(), inInit.end(), (*loss)[1].begin());

			this->Backward();
			//std::vector<std::vector<thrust::device_ptr<float>>> statesGrad = encoder.getLastStateGrad(0,2);
			//thrust::copy_n(statesGrad[0][0],4, finalOutput.begin());
			encoder.GetWeightGrads( Wx,  Wy,  Wh , 0);

			float dwxres[] = {-6.20166319,  7.9824626,
			 -9.00715791 ,9.15324942};
			thrust::copy(dwxres, dwxres+4, finalOutput.begin());
			thrust::transform(finalOutput.begin(), finalOutput.end(), Wx.begin(), finalOutput.begin(), helper::abs_diff());
			assert(1e-3 > thrust::reduce(finalOutput.begin(), finalOutput.end()));


		}
};
class LSTMTT_test: public LSTMTT_t
{
public:
	using LSTMTT_t::LSTMTT_t;
	void ForwardTest()
	{
			thrust::device_vector<float> indata_data;
			matop::matSequence_t indata;

			layer_t::InitDataAndMatrix(matop::shape_t{2,2},2, indata_data, indata);
			matop::matSequence_t* in = &indata;
			this->Init(2, in, in, true, 2);
			layer_t::initStaticHelpers(numLags, hiddenNeurons, batchSize, numOrders, false, false, true, std::vector<int>(TT_ranks.begin()+1, TT_ranks.end()-1)); //inistatic helpers does not take full ranks

			thrust::device_vector<float> inInit = helper::linspace(-1,1, batchSize*2*hiddenNeurons);
			thrust::copy(inInit.begin(), inInit.end(), h[-2].begin());

			thrust::counting_iterator<float> counter(1);
			thrust::device_vector<float> wInit = helper::linspace(-0.1, 0, 15);
			cublasHandle_t cublasHandle;
			cublasCreate(&cublasHandle);
			matop::matrix_t whmat = wh;
			//basically a two dimensional copy
			matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, whmat.rows, 3,1.f,
					wInit.data(), 3,
					whmat.data(), whmat.ld, 0.f,
					whmat.data(), whmat.ld );
			wInit = helper::linspace(0, 1, 45);
			matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, whmat.rows, 9,1.f,
								wInit.data(),9,
								whmat.data()+3, whmat.ld, 0.f,
								whmat.data()+3, whmat.ld );
			wInit = helper::linspace(0, 1, 120);
			matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, whmat.rows, 24, 1.f,
								wInit.data(), 24,
								whmat.data()+12, whmat.ld, 0.f,
								whmat.data()+12, whmat.ld);
			seqCounter = 0;


			this->CreateStateVector(s, 0);
			this->ForwardState(cublasHandle, s, hLSTMTemp[0]);
			this->ActivationState(cublasHandle, hLSTMTemp[0], h[0]);

			float res[] = {-0.06251516,-0.0623716,
					 -0.04461831, -0.05015423}; //values are doubles from python
			thrust::device_vector<float> output(res, res+4);
			thrust::transform(output.begin(), output.end(),h[0].begin(), output.begin(), helper::abs_diff());
			assert(1e-3 > thrust::reduce(output.begin(), output.end()));

			thrust::copy_n(counter, 4, dh[0].begin());

			this->ActivationStateGrad(cublasHandle, dh[0], hLSTMTemp[0]);

			//this->BackwardState(cublasHandle, dh[0]);
			float res2[] = { -0.0421081,  0. ,        -0.04334475 , 0.04933391,
					  -0.08412251 , 0.   ,      -0.08659512 , 0.09760335,

					 -0.07291028 , 0.    ,     -0.07410166 , 0.56928027,
					  -0.11111813 , 0.     ,    -0.11315641 , 0.70706851};

			output = thrust::device_vector<float>(res2, res2+16);
			thrust::transform(output.begin(), output.end(),hLSTMTemp.begin(), output.begin(), helper::abs_diff());
			assert(1e-4 > thrust::reduce(output.begin(), output.end())/output.size());

			float res3[] = {0.09018917,0.17938555,
			 0.59532751, 0.75465981};
			output = thrust::device_vector<float>(res3, res3+4);
			thrust::transform(output.begin(), output.end(),dc.begin(), output.begin(), helper::abs_diff());
			assert(1e-4 > thrust::reduce(output.begin(), output.end()));


			cublasDestroy(cublasHandle);

		}
};
class RNNTT_test: public RNNTT_t
{
public:
	using RNNTT_t::RNNTT_t;
	void ForwardTest()
	{
		thrust::device_vector<float> indata_data;
		matop::matSequence_t indata;

		layer_t::InitDataAndMatrix(matop::shape_t{2,2},2, indata_data, indata);

		matop::matSequence_t* in = &indata;
		this->Init(2, in, in, true, 2);
		layer_t::initStaticHelpers(numLags, hiddenNeurons, batchSize, numOrders, false, true,false, std::vector<int>(TT_ranks.begin()+1, TT_ranks.end()-1)); //inistatic helpers does not take full ranks

		thrust::device_vector<float> inInit = helper::linspace(-1,1, batchSize*2*hiddenNeurons);
		thrust::copy(inInit.begin(), inInit.end(), h[-2].begin());

		cublasHandle_t cublasHandle;
		cublasCreate(&cublasHandle);

		thrust::counting_iterator<float> counter(1);
		thrust::device_vector<float> init(5*3);
		matop::matrix_t whmat = wh;
		thrust::copy_n(counter, init.size(), init.begin());
		matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 5,3, 1.f,
				init.data(), 3,
				whmat.data(), whmat.ld,
				0.f,
				whmat.data(), whmat.ld);
		init = thrust::device_vector<float>(5*3*3);
		thrust::copy_n(counter, init.size(), init.begin());
		matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 5,9,1.f,
				init.data(), 9,
								whmat.data()+3, whmat.ld,
								0.f,
								whmat.data()+3, whmat.ld);

		init = thrust::device_vector<float>(5*3*2);
		thrust::copy_n(counter, init.size(), init.begin());
		matop::MatAdd(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 5,6,1.f,
								init.data(), 6,
								whmat.data()+12, whmat.ld,
								0.f,
								whmat.data()+12, whmat.ld);

		seqCounter = 0;


		this->CreateStateVector(s, 0);
		this->ForwardState(cublasHandle, s, h[0]);

		float res[] = {-175690.513, -176539.241,
				  14494.513,   17108.711}; //values are doubles from python
		thrust::device_vector<float> output(res, res+4);
		thrust::transform(output.begin(), output.end(),h[0].begin(), output.begin(), helper::abs_diff());

		//calculating relative error would be better, abs diff is quite high but its okay for the accuracy comparing floats to double
		assert(1 > thrust::reduce(output.begin(), output.end()));

		thrust::copy_n(counter, 4, dh[0].begin());

		this->BackwardState(cublasHandle, s , dh[0], sgrad);
		float res2[] = { 196641.55,  517450.6 ,  838259.6  ,1159068.6 , 1479877.8,
		                    98000.08 , 244714.22 , 391428.38 , 538142.5 ,  684856.6}; //sgradient expcted
		output = thrust::device_vector<float>(res2, res2+10);
		thrust::transform(output.begin(), output.end(),sgrad.begin(), output.begin(), helper::abs_diff());

		//calculating relative error would be better, abs diff is quite high but its okay for the accuracy comparing floats to double
		assert(1.f > thrust::reduce(output.begin(), output.end())/output.size());

		float res3[] = { 15101.388  ,  16943.266  ,  18785.143  ,   1577.7959 ,
		         1731.8368 ,   1885.8776 ,   1718.0408 ,   1936.4897 ,
		         2154.9387 ,   1858.2858 ,   2141.1428 ,   2424.     ,
		         3061.7144 ,   5398.2856 ,   3200.3264 ,   5587.347  ,
		         3338.9387 ,   5776.408  ,   4073.4229 ,   5323.6616 ,
		         6573.901  ,    385.87756,    488.6997 ,    591.52185,
		          478.85715,    627.6152 ,    776.37317,    571.83673,
		          766.53064,    961.2245 ,   1058.9387 ,   1599.9183 ,
		         1154.3091 ,   1727.6852 ,   1249.6793 ,   1855.4519 ,
		         8388.1045 ,  10164.595  ,  11941.085  ,    836.6764 ,
		          983.5102 ,   1130.344  ,    969.72595,   1180.898  ,
		         1392.07   ,   1102.7755 ,   1378.2858 ,   1653.7959 ,
		         1933.7142 ,   3142.2856 ,   2068.688  ,   3324.07   ,
		         2203.6619 ,   3505.8542 , -13185.306  , -14040.07   ,
		       -14894.834  ,  -1417.3177 ,  -1490.5422 ,  -1563.7667 ,
		        -1484.618  ,  -1585.516  ,  -1686.414  ,  -1551.9183 ,
		        -1680.4897 ,  -1809.0613 ,  -2440.1633 ,  -4569.551  ,
		        -2503.207  ,  -4657.854  ,  -2566.2507 ,  -4746.157  ,
		        -8870.624  ,  -9199.137  ,  -9527.65   ,   -966.5189 ,
		         -995.73175,  -1024.9446 ,   -993.74927,  -1032.2333 ,
		        -1070.7172 ,  -1020.9796 ,  -1068.7347 ,  -1116.4897 ,
		        -1565.3877 ,  -3027.1836 ,  -1588.828  ,  -3061.4695 ,
		        -1612.2682 ,  -3095.7551}; //core1
		output = thrust::device_vector<float>(res3, res3+90);
		thrust::transform(output.begin(), output.end(),dwh.begin(), output.begin(), helper::abs_diff());
		assert(1 > thrust::reduce(output.begin(), output.end())/output.size());
		cublasDestroy(cublasHandle);

	}
};
class test
{
public:
	void sinTest()
	{
		int numLags = 3;
		int numOrders = 2;
		int hiddenSize = 8;
		int numLayers = 2;
		int batchSize = 50;

		int trainingSteps = 10000;
		int displayStep = 100;

		data_handler sinData("tests/sin_data.npy", batchSize); // tests/sin_data.npy
		int totalSequenceLength = sinData.get_shape()[0];
		int dataDim = sinData.get_shape()[1];
		int inpSteps = 12;
		int outSteps = totalSequenceLength - inpSteps;

		std::vector<int> TT_ranks(numOrders-1,2);


		network sinTestNet(numLayers, hiddenSize, numLags, numOrders, TTRNN, dataDim, dataDim,Sigmoid, TanH, TT_ranks);
		//network sinTestNet(numLayers, hiddenSize, numLags, numOrders, RNN, dataDim, dataDim,Sigmoid, TanH);

		thrust::device_vector<float> data_vec;
		matop::matSequence_t data;

		layer_t::InitDataAndMatrix(matop::shape_t{batchSize,dataDim},totalSequenceLength, data_vec, data);

		matop::matSequence_t* x = (&data);
		float* x_ptr = thrust::raw_pointer_cast(data.data());
		thrust::device_ptr<float> z = data[inpSteps].data();
		bool isTraining = true;

		matop::matSequence_t* predData = sinTestNet.Init(x, batchSize, isTraining, inpSteps, outSteps);

		size_t free = 0;
		size_t total = 0;
		cudaMemGetInfo(&free, &total);

		std::cout << "Memory used without cublasHandle "<< (float)(total-free)/total <<  std::endl;


		float stepSizeInit = 0.001;
		float decayRate = 0.8;
		float stepSize = 0;

		//calculate memory
		/*size_t memsize = 0;
		memsize += sinTestNet.GetMemInfo();
		memsize += layer_t::GetStaticMemInfo();
		memsize += (*x).size()*sizeof(float);
		cudaMemGetInfo(&free, &total);

		std::cout << "Memory used "<< (float)(total-free)/total<< " Used for RNN Data " << (float)memsize/total <<  std::endl;*/


		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		cudaEventRecord(start);

		//decoder.SetInput(y);
		float result = 0;
		for (int i =0; i<trainingSteps; i++)
		{

			stepSize = stepSizeInit * pow(decayRate, i/2000);
			sinData.next_batch(static_cast<void*>(x_ptr), std::chrono::system_clock::now().time_since_epoch().count(), Training); //next batch should give (seqLength, batchSize, dataDim)

			result += sinTestNet.Train(z, stepSize);
			if (i % displayStep == 0)
			{
				float miliseconds = 0;
				float time = 0;
				cudaEventRecord(stop);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&miliseconds, start, stop);
				time = miliseconds/(i+1);
				std::cout << "Step " << i << " ------> ";
				std::cout << "Loss " << result/displayStep ;
				std::cout << "--->Learning Rate " << stepSize;
				std::cout << "--->Average Time per Run " << time << "ms" << std::endl;
				result = 0;
			}


		}
		sinData.next_batch((void*) x_ptr, std::chrono::system_clock::now().time_since_epoch().count(), Test); //next batch should give (seqLength, batchSize, dataDim)
		result = sinTestNet.Validate(z);

		sinData.savePrediction(x_ptr, totalSequenceLength, (*predData)[1].ptr(), outSteps, "tests/sin_dataOut", batchSize);
	}

};
class RNN_test:public RNN_t
{
public:
	using RNN_t::RNN_t;
	bool test1()
	{
		float x_h[6] = {1,2,3,4,5,6};
		//int numLayers = 1;
		bool isTraining = false;
		checkCudaErrors(cudaDeviceSynchronize());


		init_Test(isTraining);
		checkCudaErrors(cudaDeviceSynchronize());
		init_weights(1.0);
		checkCudaErrors(cudaDeviceSynchronize());
		init_State();
		layer_t::initStaticHelpers(numLags, hiddenNeurons, batchSize, numOrders, true, false,false);

		thrust::copy(x_h, &x_h[6], (*x)[0].begin());

		cublasHandle_t cublasHandle;
		cublasCreate(&cublasHandle);
		activationOutput = Linear;
		activationHidden = Linear;
		this->ForwardInference(cublasHandle);
		cublasDestroy(cublasHandle);

		float result[] = {346.0,346.0, 3382.0,3382.0, 12178.0, 12178.0};
		thrust::device_vector<float> result_vec(result, result+6);
		thrust::device_vector<float> result_state(h[0].begin(),h[1].begin());
		assert(result_state==result_vec);

		checkCudaErrors(cudaDeviceSynchronize());
		//checkCudaErrors(cudaFree(x_d));
		std::cout << "Test 1 RNN complete!" << std::endl;
		return true;
	}
	bool KronProdTest()
	{
		float x_h[6] = {1,2,3,4,5,6};
		float y_h[6] = {8,9,10,11,12,13};
		thrust::device_vector<float> x_d(x_h, x_h+6);
		thrust::device_vector<float> y_d(y_h, y_h+6);
		thrust::device_vector<float> res(x_d.size()*y_d.size(),0);
		float* x_ptr = thrust::raw_pointer_cast(&x_d[0]);
		float* y_ptr = thrust::raw_pointer_cast(&y_d[0]);
		float* res_ptr = thrust::raw_pointer_cast(&res[0]);

		int rowsX = 2;
		int colsX = 3;
		int rowsY = 3;
		int colsY = 2;

		cublasHandle_t cublasHandle;
		cublasCreate(&cublasHandle);
		matop::GEMM_StridedBatched(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
				1.f, rowsX*colsX,1 , rowsY*colsY,
				x_d.data(),  1, rowsX*colsX,
				y_d.data(), 1, rowsY*colsY,
				0.f,
				res.data(), rowsY*colsY, colsX*colsY*rowsX*rowsY,
				1);
		//matop::OuterBatched(cublasHandle, &alpha, x_ptr, rowsX, colsX, 0, y_ptr, rowsY, colsY, 0, &beta, res_ptr, 0, 1 );
		float result1[36] = { 8.,  9., 10., 11., 12., 13.,
			       16., 18., 20., 22., 24., 26.,
			       24., 27., 30., 33., 36., 39.,
			       32., 36., 40., 44., 48., 52.,
			       40., 45., 50., 55., 60., 65.,
			       48., 54., 60., 66., 72., 78.};
		thrust::device_vector<float> res1(result1, result1+36);
		assert(res == res1);


		cublasDestroy(cublasHandle);
		return true;
	}
	bool HiddenToHiddenGradientTest()
		{
			//init weights
			thrust::counting_iterator<float> counter(1);
			thrust::copy_n(counter, wh.size(), wh.begin());

			//int sizeOfStateTensorGrad = higherOrderStateSize/sizeOfOneStateVector;
			cublasHandle_t cublasHandle;
			cublasCreate(&cublasHandle);
			float h1[6] = {2,3,5,6,2,3}; float h2[6] = {4,5,7,8,4,5};
			thrust::copy(h1, h1+6, h[-1].begin());
			thrust::copy(h2, h2+6, h[-2].begin());

			layer_t::InitDataAndMatrix(matop::shape_t{batchSize, hiddenNeurons}, numLags+1, dh_data, dh);

			float dh0[6] = {0.5,0.75, 0.5,0.75,0.5,0.75};
			thrust::copy(dh0, dh0+6, dh[0].begin());
			this->CreateStateVector(s, 0);

			matop::GEMM(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_T,
					1.f,
					dh[0],
					wh,
					0.f,
					stateTensor
					);

			this->StateGradient(cublasHandle, stateTensor, sgrad, -1);
			this->ScatterStateVector(cublasHandle, sgrad, -1);

			//float dh1Res[6] = {99612.5, 126975., 382980.5, 432393.,99612.5, 126975.};
			//float dh2Res[6] = {154337.5, 181700., 481805.5, 531218. ,154337.5, 181700.};
			float dhRes[18] = {0.5,0.75, 0.5,0.75,0.5,0.75
					, 154337.5, 181700., 481805.5, 531218. ,154337.5, 181700.
					, 99612.5, 126975., 382980.5, 432393.,99612.5, 126975. };
			thrust::device_vector<float> res(dhRes, dhRes+18);
			thrust::device_vector<float> dh_asvec(dh[0].data(), dh[0].data()+18);
			assert(res == dh_asvec);

			return true;
		}
private:
	float* x_d;
	thrust::device_vector<float> x_vec;
	matop::matSequence_t x_seq;
	thrust::device_vector<float> x_seq_data;
	matop::matSequence_t* outData;

	float returnWeight(int u, int i, int j, int k)
	{
		return k+(j-1)*5+(i-1)*25+(u-1)*125;
	}
	//creates weights Gradient matrix for weights in 1-SIZE(weights)
	void createWeightsGradient(thrust::device_vector<float>& weights)
	{
		for (int i = 0; i < 5; ++i)
			for(int j= 0; j < 5; ++j)
			{
				weights[j+5*i] = returnWeight(1,2, i+1, j+1) + returnWeight(1, i+1,2, j+1) + returnWeight(1,i+1, j+1,2);
				weights[25+j+5*i] = returnWeight(1,3, i+1, j+1) + returnWeight(1, i+1,3, j+1) + returnWeight(1,i+1, j+1,3);
				weights[50+j+5*i] = returnWeight(2,2, i+1, j+1) + returnWeight(2, i+1,2, j+1) + returnWeight(2,i+1, j+1,2);
				weights[75+j+5*i] = returnWeight(2,3, i+1, j+1) + returnWeight(2, i+1,3, j+1) + returnWeight(2,i+1, j+1,3);
			}
	}


	void init_State()
	{
		float h_h[12] = {1,2,3,4,5,6,1,2,3,4,5,6};
		//thrust::copy_n(s_h, s.size(), s.begin());
		thrust::copy_n(h_h, h.sizePerElem*(numLags), h[1].begin() );

	}
	void init_weights(float initVal)
	{
		thrust::device_ptr<float> xdptr(x_d);

		thrust::fill(wx.begin(), wx.end(), initVal);
		thrust::fill(wh.begin(), wh.end(), initVal);
		if(!wy.empty()) thrust::fill(wy.begin(), wy.end(), initVal);
	}
	void init_Test(bool isTraining)
	{
		int batchSize = 3;



		//x_vec = thrust::device_vector<float>(batchSize*inputNeurons);
		layer_t::InitDataAndMatrix(matop::shape_t{batchSize, inputNeurons},1, x_seq_data, x_seq);
		//x_seq = matop::matSequence_t(batchSize*inputNeurons, 1);
		//checkCudaErrors(cudaMalloc(&x_d, batchSize*dataDim*sizeof(float)));
		//thrust::device_ptr<float> inData = &x_vec[0];

	    matop::matSequence_t* inData(&x_seq);
		matop::matSequence_t* gradData;

		this->Init(batchSize, inData, gradData, isTraining, seqLength);

		outData = inData;

	}

};

int main(){
    unsigned int batch_size = 20;
    data_handler data_f("tests/a_f.npy",batch_size);
    data_handler data_c("tests/a_c.npy",batch_size);

    std::vector<unsigned int> shape = data_f.get_shape();
    std::vector<unsigned int> set_size = data_f.get_setsizes();
    size_t word_size = data_f.get_wordsize();

    assert(shape.size()== 2);
    assert(shape[0] == 4);
    assert(shape[1] == 2);

    assert(set_size.size() == 3);
    assert(set_size[0] == 40 );
    assert(set_size[1] == 5 );
    assert(set_size[2] == 5 );

    float* dst_f = NULL;
    float* dst_c = NULL;
    /*if (word_size==sizeof(float))
        {
            static (float)*dst_f = NULL;
            static (float)*dst_c = NULL;
        }*/
    unsigned int size = batch_size*shape[0]*shape[1];
    checkCudaErrors(cudaMalloc(&dst_f, size*word_size));
    checkCudaErrors(cudaMalloc(&dst_c, size*word_size));

    cublasHandle_t cublasHandle;
    checkCublasErrors(cublasCreate(&cublasHandle));

    bool epoche_finish = false;
    while (!epoche_finish)
    {
        if ((data_f.next_batch((void*)dst_f,1, Training)) & (data_c.next_batch((void*)dst_c,1, Training))) epoche_finish = true;
        std::cout << "Next Batch Test ";
        float sum_f = -1;
        float sum_c = -1;
        checkCublasErrors(cublasSasum(cublasHandle,size, dst_f, 1, &sum_f));
        checkCublasErrors(cublasSasum(cublasHandle,size, dst_c, 1, &sum_c));
        assert(sum_f > 0.0);
        assert(sum_c > 0.0);
        assert(sum_f == sum_c);
        float alpha = -1;
        checkCublasErrors(cublasSaxpy(cublasHandle, size, &alpha, dst_f, 1, dst_c, 1 ));
        checkCublasErrors(cublasSasum(cublasHandle, size, dst_c, 1, &sum_c));
        assert(sum_c == 0);
        cudaDeviceSynchronize();
        std::cout << "----Okay" << std::endl;
    }
    cublasDestroy(cublasHandle);

    int numOrders = 3;
    int dataDim = 2;
    int hiddenSize = 2;
    int numLags = 2;

    checkCudaErrors(cudaDeviceSynchronize());
    RNN_test RNNT(dataDim, hiddenSize, 0, numLags, numOrders,Linear, Linear);
    //layer_t::initStaticHelpers(numLags,hiddenSize,3,numOrders);
    //~data_f;
    //~data_c;
    assert(RNNT.test1());
    assert(RNNT.KronProdTest());

    assert(RNNT.HiddenToHiddenGradientTest());
    assert(RNNT.HiddenToHiddenGradientTest());
    assert(RNNT.HiddenToHiddenGradientTest());

    networkTest net(2, 2, 2, 2, RNN, 2, 2,Linear, Linear );
    net.ForwardBackWard();

    std::vector<int> TT_ranks(2,3);
    RNNTT_test RNNTT(dataDim, hiddenSize, 0, numLags, numOrders,Linear, Linear, TT_ranks);
    RNNTT.ForwardTest();

    LSTMTT_test LSTMTT(dataDim, hiddenSize, 0, numLags, numOrders,Linear, Linear, TT_ranks);
    LSTMTT.ForwardTest();

    networkTestLSTM net2(2, 2, 2, 3, TTLSTM, 2, 2,Linear, Linear, TT_ranks );
    net2.ForwardBackWard();

    test testing;
    testing.sinTest();

    //assert(RNN.test2());

    cudaFree(dst_f);
    cudaFree(dst_c);
    cudaDeviceSynchronize();

}
