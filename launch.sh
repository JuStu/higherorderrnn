#! /bin/sh
datapath='/home/julian/Desktop/PowerFolders/DataSets/roessler.npy'
savepath='/home/julian/Desktop/PowerFolders/DataSets/lorenz_'
plotter='/home/julian/Documents/CSE/Studienarbeit/Helpers/Plotter/plotter.py'
mkdir -p $savepath
options0='-l 2 -o 2 -b 50 -h 8 -s 8 -r 2 -m 3 -a 0.001 -x 5 -y 5 -d 500 '
options1='-l 2 -o 3 -b 50 -h 8 -s 16 -r 2,9 -m 10000 -a 0.001 -x 5 -d 500'
options2='-l 2 -o 2 -b 50 -h 16 -s 64 -r 36 -m 10000 -a 0.001 -x 5 -d 500'
options3='-l 2 -o 4 -b 50 -h 8 -s 8 -r 6,100,52 -m 10000 -a 0.0001 -x 5 -d 500'
options4='-l 2 -o 2 -b 50 -h 8 -s 4 -r 2 -m 10000 -a 0.001 -x 5 -d 500'
options5='-l 2 -o 2 -b 50 -h 8 -s 4 -r 1 -m 10000 -a 0.001 -x 5 -d 10000'
options6='-l 2 -o 2 -b 50 -h 4 -s 4 -r 2 -m 10000 -a 0.001 -x 5 -d 10000'
options7='-l 2 -o 2 -b 50 -h 8 -s 8 -r 2 -m 10000 -a 0.001 -x 5 -d 500'
options8='-l 2 -o 2 -b 50 -h 8 -s 8 -r 1 -m 10000 -a 0.001 -x 5 -d 500'
options9='-l 2 -o 2 -b 50 -h 8 -s 8 -r 2 -m 10000 -a 0.001 -x 5 -d 500 -n'
options10='-l 2 -o 2 -b 50 -h 16 -s 8 -r 2 -m 10000 -a 0.001 -x 5 -d 500'
options11='-l 2 -o 2 -b 50 -h 4 -s 8 -r 1 -m 10000 -a 0.001 -x 5 -d 500'
options12='-l 2 -o 2 -b 50 -h 8 -s 8 -r 3 -m 10000 -a 0.001 -x 5 -d 500 -n'
options13='-l 2 -o 2 -b 50 -h 8 -s 8 -r 4 -m 10000 -a 0.001 -x 5 -d 500 -n'
options14='-l 2 -o 2 -b 50 -h 8 -s 8 -r 3 -m 20000 -a 0.001 -x 5 -d 500 -n'
options15='-l 2 -o 1 -b 50 -h 8 -s 8 -m 10000 -a 0.001 -x 5 -d 500'
options16='-l 2 -o 1 -b 50 -h 4 -s 8 -m 10000 -a 0.001 -x 5 -d 500'
options17='-l 2 -o 1 -b 50 -h 8 -s 8 -m 10000 -a 0.001 -x 5 -d 500 -n'
options18='-l 2 -o 2 -b 50 -h 8 -s 8 -r 4 -m 10000 -a 0.0001 -x 5 -d 500'
options19='-l 2 -o 2 -b 50 -h 8 -s 16 -r 8 -m 10000 -a 0.0001 -x 5 -d 500'
options20='-l 2 -o 3 -b 50 -h 8 -s 8 -r 4,16 -m 10000 -a 0.0001 -x 5 -d 500'
options21='-l 2 -o 2 -b 50 -h 8 -s 32 -r 16 -m 10000 -a 0.0001 -x 5 -d 500'
options22='-l 2 -o 2 -b 50 -h 8 -s 32 -r 64 -m 10000 -a 0.0001 -x 5 -d 500'
options23='-l 2 -o 3 -b 50 -h 8 -s 16 -r 8,32 -m 10000 -a 0.0001 -x 5 -d 500'
options24='-l 2 -o 2 -b 50 -h 12 -s 16 -r 8 -m 10000 -a 0.0001 -x 5 -d 500'
options25='-l 2 -o 2 -b 50 -h 12 -s 32 -r 12 -m 10000 -a 0.0001 -x 5 -d 500'
options26='-l 2 -o 2 -b 50 -h 12 -s 16 -r 8 -m 10000 -a 0.0001 -x 20 -d 500'
options27='-l 2 -o 3 -b 50 -h 12 -s 16 -r 8,16 -m 10000 -a 0.0001 -x 20 -d 500'
options28='-l 2 -o 2 -b 50 -h 12 -s 16 -r 8 -m 20000 -a 0.0001 -x 20 -d 500'
options29='-l 2 -o 2 -b 50 -h 2 -s 4 -r 3 -m 2000 -a 0.001 -x 5 -d 2000 -n'
options30='-l 1 -o 6 -b 50 -h 8 -s 8 -r 4,4,4,4,4 -m 10000 -a 0.001 -x 20 -y 20 -d 2000 -n'
options31='-l 1 -o 5 -b 50 -h 8 -s 32 -r 16,16,16,16 -m 10000 -a 0.001 -x 20 -y 40 -d 500 -n'
options32='-l 1 -o 5 -b 50 -h 8 -s 32 -r 32,32,32,32 -m 10000 -a 0.001 -x 20 -y 40 -d 2000 -n'
options33='-l 1 -o 5 -b 50 -h 8 -s 32 -r 16,16,16,16 -m 10000 -a 0.001 -x 20 -y 60 -d 500 -n'
options34='-l 1 -o 5 -b 50 -h 8 -s 64 -r 32,32,32,32 -m 10000 -a 0.001 -x 20 -y 40 -d 2000 -n'
options35='-l 1 -o 6 -b 50 -h 8 -s 32 -r 16,16,16,16,16 -m 10000 -a 0.001 -x 20 -y 60 -d 2000 -n'
options36='-l 1 -o 5 -b 50 -h 8 -s 64 -r 16,16,32,32 -m 10000 -a 0.001 -x 20 -y 40 -d 2000 -n'
options37='-l 1 -o 5 -b 50 -h 8 -s 64 -r 16,16,16,16 -m 10000 -a 0.001 -x 20 -y 60 -d 2000 -n'
options38='-l 1 -o 5 -b 50 -h 8 -s 32 -r 16,16,16,16 -m 10000 -a 0.001 -x 20 -y 60 -d 500'
options39='-l 1 -o 5 -b 50 -h 16 -s 16 -r 8,8,8,8 -m 9000 -a 0.001 -x 20 -y 60 -d 500 -n' #good
options40='-l 1 -o 5 -b 50 -h 16 -s 16 -r 16,16,16,16 -m 10000 -a 0.001 -x 20 -y 60 -d 500 -n'
options41='-l 1 -o 5 -b 50 -h 32 -s 16 -r 8,8,8,8 -m 2000 -a 0.001 -x 20 -y 80 -d 500 -n' #good once
options42='-l 1 -o 2 -b 50 -h 32 -s 16 -r 8 -m 10000 -a 0.001 -x 20 -y 80 -d 500 -n' # good but time offset
options43='-l 1 -o 5 -b 50 -h 32 -s 16 -r 8,8,8,8 -m 10000 -a 0.0001 -x 20 -y 80 -d 500 -n' #good
options44='-l 1 -o 3 -b 50 -h 32 -s 16 -r 8,8 -m 10000 -a 0.001 -x 20 -y 80 -d 500 -n' #good
options45='-l 1 -o 3 -b 50 -h 32 -s 16 -r 8,8 -m 10000 -a 0.001 -x 20 -y 100 -d 500 -n' #end not good
options46='-l 1 -o 5 -b 50 -h 32 -s 16 -r 8,8,8,8 -m 10000 -a 0.0001 -x 20 -y 100 -d 500 -n' #end not good
options47='-l 1 -o 3 -b 50 -h 64 -s 16 -r 8,8 -m 10000 -a 0.001 -x 20 -y 100 -d 500 -n' #okay
options48='-l 1 -o 5 -b 50 -h 64 -s 16 -r 8,8,8,8 -m 10000 -a 0.0001 -x 20 -y 100 -d 500 -n' #bit better
options49='-l 1 -o 5 -b 50 -h 64 -s 8 -r 4,4,4,4 -m 20000 -a 0.001 -x 20 -y 100 -d 500 -n' #best i think
options50='-l 1 -o 6 -b 50 -h 64 -s 8 -r 4,4,4,4,4 -m 40000 -a 0.0001 -x 20 -y 100 -d 500 -n' #same
options51='-l 1 -o 4 -b 50 -h 64 -s 8 -r 4,4,4 -m 20000 -a 0.001 -x 20 -y 100 -d 500 -n' # same
options52='-l 1 -o 5 -b 50 -h 64 -s 16 -r 8,8,8,8 -m 20000 -a 0.0001 -x 20 -y 100 -d 500 -n' #bit better
options53='-l 1 -o 5 -b 50 -h 64 -s 8 -r 4,4,4,4 -m 100000 -a 0.0001 -x 20 -y 100 -d 500 -n' #best i think
options54='-l 1 -o 5 -b 50 -h 32 -s 8 -r 4,4,4,4 -m 20000 -a 0.0001 -x 20 -y 100 -d 500 -n' 
options55='-l 1 -o 5 -b 50 -h 64 -s 16 -r 8,8,8,8 -m 100000 -a 0.0001 -x 20 -y 100 -d 500 -n' #best 0.05
options56='-l 1 -o 5 -b 50 -h 64 -s 3 -r 1,1,1,1 -m 100000 -a 0.0001 -x 20 -y 100 -d 500 -n' 
options57='-l 1 -o 3 -b 50 -h 64 -s 16 -r 8,8 -m 100000 -a 0.001 -x 20 -y 100 -d 500 -n' #best 0.048
options58='-l 1 -o 2 -b 50 -h 64 -s 16 -r 8 -m 100000 -a 0.001 -x 20 -y 100 -d 500 -n' # best 0.045
options59='-l 2 -o 2 -b 50 -h 64 -s 3 -r 8 -m 100000 -a 0.001 -x 20 -y 100 -d 500 -n' 

#./main $datapath $savepath+'17' $options17
datapath='/home/julian/Desktop/PowerFolders/DataSets/lorenz_data_150noTime.npy'
savepath='/home/julian/Desktop/PowerFolders/DataSets/lorenz_'
#./main $datapath $savepath+'31' $options31
#python3 $plotter $savepath+'31_pred.npy' $savepath+'31_in.npy' $datapath
#./main $datapath $savepath+'40' $options40
#python3 $plotter $savepath+'48_pred.npy' $savepath+'48_in.npy' $datapath
#python3 $plotter $savepath+'51_pred.npy' $savepath+'51_in.npy' $datapath


#echo $options58
#python3 $plotter $savepath+'58_pred.npy' $savepath+'58_in.npy' $datapath
#./main $datapath $savepath+'58' $options58
#echo $options59
#./main $datapath $savepath+'59' $options59
#./main $datapath $savepath+'50' $options50
python3 $plotter $savepath+'58_pred.npy' $savepath+'58_in.npy' $datapath
python3 $plotter $savepath+'55_pred.npy' $savepath+'55_in.npy' $datapath
#python3 $plotter $savepath+'50_pred.npy' $savepath+'50_in.npy' $datapath
#cuda-memcheck ./main $datapath $savepath'0' $options0
#./main $datapath $savepath+'0' $options0
#python3 $plotter $savepath+'0_pred.npy' $savepath+'0_in.npy' $datapath
#./main $datapath $savepath+'1' $options1
#python3 $plotter $savepath+'1_pred.npy' $savepath+'1_in.npy' $datapath
#./main $datapath $savepath+'2' $options2
#python3 $plotter $savepath+'2_pred.npy' $savepath+'2_in.npy' $datapath
#./main $datapath $savepath+'3' $options3
#python3 $plotter $savepath+'3_pred.npy' $savepath+'3_in.npy' $datapath
#./main $datapath $savepath+'4' $options4
#./main $datapath $savepath+'5' $options5
#./main $datapath $savepath+'6' $options6
#./main $datapath $savepath+'7' $options7
#./main $datapath $savepath+'8' $options8
#./main $datapath $savepath+'9' $options9
#./main $datapath $savepath+'10' $options10
#./main $datapath $savepath+'11' $options11
#./main $datapath $savepath+'12' $options12
#./main $datapath $savepath+'13' $options13
#./main $datapath $savepath+'14' $options14
#./main $datapath $savepath+'15' $options15
#./main $datapath $savepath+'16' $options16
#./main $datapath $savepath+'17' $options17
datapath='/home/julian/Desktop/PowerFolders/DataSets/lorenz_splitSet100s.npy'
savepath='/home/julian/Desktop/PowerFolders/DataSets/lorenz_split'
#./main $datapath $savepath+'7' $options7
#./main $datapath $savepath+'18' $options18
#./main $datapath $savepath+'19' $options19
#./main $datapath $savepath+'20' $options20
#./main $datapath $savepath+'21' $options21
#./main $datapath $savepath+'22' $options22
#./main $datapath $savepath+'23' $options23
#./main $datapath $savepath+'24' $options24
#./main $datapath $savepath+'25' $options25
#./main $datapath $savepath+'26' $options26
#./main $datapath $savepath+'27' $options27
#./main $datapath $savepath+'28' $options28
datapath='/home/julian/Desktop/PowerFolders/DataSets/roesslerSplitset.npy'
savepath='/home/julian/Desktop/PowerFolders/DataSets/roessler_split'
#./main $datapath $savepath+'25' $options25
#./main $datapath $savepath+'26' $options26
datapath='/home/julian/PowerFolders/DataSets/lorenz_data_150.npy'
savepath='/home/julian/PowerFolders/DataSets/lorenz_speed'
#./main $datapath $savepath+ss $options29
