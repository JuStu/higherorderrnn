/*
 * helper.h


 *
 *  Created on: Feb 6, 2020
 *      Author: julian
 */
#ifndef HELPER_H
#define HELPER_H
#include <thrust/random.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/functional.h>
#include <thrust/fill.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <chrono>
namespace helper
{
    const int TILE_DIM = 32;
    void CreateVec(float* hfirstpart, float* hendpart, int hsize, int numLagsfirst, int numLags, int batchSize, float* sstart);
    __global__ void CreateVecCore(float* hfirstpart, float* hendpart, int hsize, int numLagsfirst, int numLags, int batchSize, float* sstart);
	//https://stackoverflow.com/questions/18705190/purpose-and-usage-of-counting-iterators-in-cuda-thrust-library
	thrust::device_vector<float> linspace(float a, float b, float N);
	__forceinline__ __device__ float sigmoidf(float in) {
	   return 1.f / (1.f + expf(-in));
	}
	struct tanh_f
	{
		 __device__ float operator()(float x)
		{
			 return  tanhf(x);
		}
	};
	struct abs_diff
	{
		__device__ float operator()(float x, float y)
		{
			return fabs(x-y);
		}
	};
	struct tanh_fg
		{
			 __device__ float operator()(float y, float dy)
			{
				 return  dy * (1-powf(y,2));
			}
		};

	struct sigmoid_f
	{
	 __device__ float operator()(float x)
	 {
	 		        		return  sigmoidf(x);
	 	}
	};
	struct sigmoid_fg
	{
		__device__ float operator()(float y, float dy)
		{
			return dy*y*(1-y);
		}
	};
	struct inequal
	{
		//const float op;
		inequal() {}
		__device__ bool operator()(float i)
			{
				return (i != 0);
			}
	};
	struct equal
		{
			//const float op;
			equal() {}
			__device__ bool operator()(float i)
				{
					return (i == 0);
				}
		};
	struct greater
		{
			//const float op;
			greater() {}
			__device__ bool operator()(float i)
				{
					return (i > 1);
				}
		};
	struct Adam
	{
	    typedef thrust::tuple<float,float,float,float> Float4;
	    Adam(float _moment1, float _moment2, int _t, float _alpha): moment1(_moment1), moment2(_moment2), t(_t), alpha(_alpha){}
	    __device__
	    Float4 operator()(Float4 in)
	    {
	        float g = thrust::get<0>(in);
	        float m = moment1*thrust::get<1>(in) + (1-moment1)*g;
	        float v = moment2*thrust::get<2>(in) + (1-moment2)*powf(g,2);
	        //float alpha_s = alpha * sqrtf(1-powf(moment2,t))/(1-powf(moment1,t));
	        float m_h = m/(1-powf(moment1,t));
	        float v_h = v/(1-powf(moment2,t));
	        float w_new = thrust::get<3>(in) - alpha * m_h/(sqrtf(v_h)+1e-8);
	        return thrust::make_tuple(0,m,v,w_new); 
	    }
	    private:
	        float moment1;
	        float moment2;
	        int t;
	        float alpha;
	    
	};
	struct saxpy_functor
	{
	    const float a;
	    const float b;
	    saxpy_functor(float _a, float _b) : a(_a), b(_b) {}

	    __device__
	        float operator()(const float& x, const float& y) const {
	            return b * y + a*x;
	        }
	};
	struct c_update
	{
			typedef thrust::tuple<float,float,float,float> Float4;
			//order = c_old, f, i, j
			__device__
			float operator()(Float4 in)
			{
				return thrust::get<0>(in)*thrust::get<1>(in)+thrust::get<2>(in)*thrust::get<3>(in);
			}
		};
	struct c_grad
		{
				typedef thrust::tuple<float,float,float,float> Float4;
				//order = c_old, f, i, j
				__device__
				float operator()(Float4 in)
				{
					return thrust::get<0>(in)*thrust::get<1>(in)*(1-powf(tanhf(thrust::get<2>(in)),2))+thrust::get<3>(in);
				}
			};
	struct isnan_test {
	    __host__ __device__ bool operator()(const float a) const {
	        return isnan(a);
	    }
	};
	struct h_update
			{
				__device__
				float operator()(float c, float out)
				{
					return out*tanh(c);
				}
			};
	struct glorot_init
	{
		thrust::default_random_engine gen;
		thrust::normal_distribution<float> dist;
		glorot_init(int stateVecSize, int r_act, int r_prev, unsigned int seed): gen(thrust::default_random_engine((float)seed)),
				dist(0, sqrtf(2/(float)(r_act*stateVecSize+r_prev*stateVecSize)))  {}
		__device__
		float operator()()
		{
			unsigned int threadId = blockIdx.x*blockDim.x+threadIdx.x;
			//unsigned int seed = hash(threadId);
			gen.discard(threadId);
			return dist(gen);
		}
	};
	struct xavier_init
	{
		thrust::default_random_engine gen;
		thrust::normal_distribution<float> dist;
		xavier_init(int _firstDim, unsigned int seed): gen(thrust::default_random_engine((float)seed)),
				dist(0, sqrtf(1/(float)_firstDim))  {}
		__device__
		float operator()()
		{
			unsigned int threadId = blockIdx.x*blockDim.x+threadIdx.x;
			//unsigned int seed = hash(threadId);
			gen.discard(threadId);
			return dist(gen);
		}

	};
	struct movingAverageSq
	{
		const int batchSize;
		const float beta;
		movingAverageSq(int _bSize, float _beta): batchSize(_bSize),beta(_beta) {}
		__device__
		float operator()(float x, float x_new)
		{
			return beta*x + (1-beta)*powf(x_new/batchSize,2);
		}
	};
	struct RMSPropUpdate
	{
		//alpha = learningRate / batchSize
		const float alpha;
		//(w,average,dw)
		typedef thrust::tuple<float,float,float> Float3;
		RMSPropUpdate(float a): alpha(a) {}
		__device__
		float operator()(Float3 in)
		{
			float val = thrust::get<0>(in)  + (alpha / (sqrtf(thrust::get<1>(in))+1e-7))*thrust::get<2>(in);
			return  val; //isnan(val) ? thrust::get<0>(in)  + thrust::get<2>(in)  : val;
		}
	};
	template <typename Iterator>
	class strided_range
	{
	    public:

		typedef typename thrust::iterator_difference<Iterator>::type difference_type;

	    struct stride_functor : public thrust::unary_function<difference_type,difference_type>
	    {
	        difference_type stride;

	        stride_functor(difference_type stride)
	            : stride(stride) {}

	        __host__ __device__
	        difference_type operator()(const difference_type& i) const
	        {
	            return stride * i;
	        }
	    };

	    typedef typename thrust::counting_iterator<difference_type>                   CountingIterator;
	    typedef typename thrust::transform_iterator<stride_functor, CountingIterator> TransformIterator;
	    typedef typename thrust::permutation_iterator<Iterator,TransformIterator>     PermutationIterator;

	    // type of the strided_range iterator
	    typedef PermutationIterator iterator;

	    // construct strided_range for the range [first,last)
	    strided_range(Iterator first, Iterator last, difference_type stride)
	        : first(first), last(last), stride(stride) {}

	    iterator begin(void) const
	    {
	        return PermutationIterator(first, TransformIterator(CountingIterator(0), stride_functor(stride)));
	    }

	    iterator end(void) const
	    {
	        return begin() + ((last - first) + (stride - 1)) / stride;
	    }

	    protected:
	    Iterator first;
	    Iterator last;
	    difference_type stride;
	};
}
#endif



