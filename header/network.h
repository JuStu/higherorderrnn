#ifndef NETWORK_H
#define NETWORK_H
#include <vector>
#include "layer.h"
enum network_type {RNN, TTRNN, TTLSTM};
class network
{
    protected:
		struct unit_t
		{
		private:
			std::vector<layer_t*> layers;
			int numLayers;
		public:
			void ForwardTraining(const cublasHandle_t& cublasHandle, int seqStart, int seqEnd);
			void SetInput(matop::matSequence_t* input, matop::matSequence_t* inputgrad);
			unit_t(int _num_layer,
			                int _hidden_size,
			                int _num_lags,
			                int _numOrders,
			                network_type _net_type,
			                int inputSize,
			                int outputSize,
			                activationType actOut,
			                activationType actHidden,
			                std::vector<int> _TT_ranks = std::vector<int>());
			void Init(matop::matSequence_t* dataIn, matop::matSequence_t*& dataOut, matop::matSequence_t*&dataGrad, int _batchSize, bool isTraining,int seqLength);
			std::vector<RNNState> getLastState(int seqNum, int _numLags) const;
			void setInitialStates(std::vector<RNNState> states);
			std::vector<MemState> getLastMemory(int seqNum) const;
			void setInitialMemory(std::vector<MemState> states);
			void Backward(const cublasHandle_t& cublasHandle);
			void UpdateWeights(float learningRate, int stepNum);
			void resetStates();
			void resetStateGrad();
			size_t GetMemInfo();
			void setInitialStateGrad(std::vector<RNNState> states, int seqNum);
			std::vector<RNNState> getLastStateGrad(int seqNum, int _numLags) const;
			void setInitialMemoryGrad(std::vector<MemState> states);
			std::vector<MemState> getLastMemoryGrad(int seqNum) const;
			void SetWeights(matop::matrix_t _Wx, matop::matrix_t _Wy, matop::matrix_t _Wh, matop::matrix_t _by );
			void GetWeightGrads(matop::matrix_t& _Wx, matop::matrix_t& _Wy, matop::matrix_t& _Wh , int layerNum);
		};
		unit_t encoder;
		unit_t decoder;
		matop::matSequence_t* inData;
		matop::matSequence_t* outData;
		matop::matSequence_t* loss;
        int numLayers;
        int numLags;
        int numOrders;
        int hiddenSize;
        int batchSize;
        int inpSteps;
        int outSteps;
        int stepNum;
        std::vector<int> TT_ranks;
        network_type net_type;
        int inputSize;
        int outputSize;

        cublasHandle_t cublasHandle;
        void LossGradient(thrust::device_ptr<float> targetValues, int seqStart, int seqEnd);
        float Loss(const cublasHandle_t& cublasHandle);
    public:
        network(int _num_layer,
                int _hidden_size,
                int _num_lags,
                int _numOrders,
                network_type _net_type,
                int inputSize,
                int outputSize,
                activationType actOut = Sigmoid,
                activationType actHidden = TanH,
                std::vector<int> _TT_ranks = std::vector<int>());
        ~network();
        matop::matSequence_t* Init(matop::matSequence_t* dataIn, int _batchSize, bool isTraining,int _inSteps, int _outSteps);

        int getTensorSize();

        float Train(thrust::device_ptr<float> targetValues, float stepSize);
        void Backward();
        void Forward();
        void UpdateWeights(float stepSize, int stepNum);
        float Validate(thrust::device_ptr<float> targetValue);


        size_t GetMemInfo();
};
#endif
