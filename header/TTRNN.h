/*
 * TTRNN.h

 *
 *  Created on: Jun 20, 2020
 *      Author: julian
 */
#ifndef TTRNN_H_
#define TTRNN_H_
#include "layer.h"
#include "matop.h"
class RNNTT_t: public layer_t
{
protected:
	std::vector<int> TT_ranks;
	int TTRanksStacked; //column Size of matrix out of all TT
	matop::matSequence_t TTStates; //state vector in TT Format
	thrust::device_vector<float> TTStates_data;
	std::vector<matop::matSequence_t> TTSubResults; //sub results for calculation
	thrust::device_vector<float> TTSubResults_data; //vector of device_vectors turn bad cause somehow the device memory gets freed when we use push-back
	int TTStride(int coreNum);

	virtual void InitLayerSpecific() override;
public:
	RNNTT_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut, activationType _actTypeHidden, std::vector<int> _TT_ranks);
	virtual void ForwardState(const cublasHandle_t& cublasHandle, const matop::matrix_t& stateInput, matop::matrix_t& stateOutput) override;
	virtual void BackwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& _s,const matop::matrix_t& stateGrad, matop::matrix_t& _sgrad) override;

};
#endif
