#ifndef TTLSTM_H_
#define TTLSTM_H_
#include "TTRNN.h"
class LSTMTT_t: public RNNTT_t
{
protected:
	const int numUnits;

	matop::matSequence_t hLSTM; //hLSTM is a array of numUnits*batchSize*hiddenSize, with numUnits = [input, forget, output, new_input] gate
	thrust::device_vector<float> hLSTM_data;

	matop::matSequence_t dhLSTM;
	thrust::device_vector<float> dhLSTM_data;

	matop::matSequence_t c; //internal memory
	thrust::device_vector<float> c_data; //internal memory

	matop::matrix_t dc;
	thrust::device_vector<float> dc_data;

	virtual void InitLayerSpecific() override;

public:
	LSTMTT_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut, activationType _actTypeHidden, std::vector<int> _TT_ranks);

	/*virtual void ForwardInference(const cublasHandle_t& cublasHandle) override;
	virtual void BackwardTraining(const cublasHandle_t& cublasHandle) override;*/
	virtual void ActivationState(const cublasHandle_t& cublasHandle,  matop::matrix_t& input, matop::matrix_t& output)override;
	virtual void ActivationStateGrad(const cublasHandle_t& cublasHandle, const matop::matrix_t& inputGrad, matop::matrix_t& outputGrad)override;
	//void LSTMStepForward(const cublasHandle_t& cublasHandle);
	//void LSTMStepBackward(const cublasHandle_t& cublasHandle);

	virtual MemState getLastMemory(int seqNum)override;
	virtual MemState getLastMemoryGrad(int seqNum)override;
	virtual void setInitialMemory(const MemState state)override;
    virtual void setInitialMemoryGrad(const MemState state)override;
    virtual void resetStateGrad() override;
    virtual void resetStates()override;
};
#endif
