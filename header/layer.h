#ifndef LAYER_H
#define LAYER_H
#include <iostream>
#include <cublas_v2.h>
#include <cuda.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/fill.h>
#include <thrust/logical.h>
#include <cmath>
#include <tuple>
#include <thrust/execution_policy.h>
#include <thrust/transform.h>
#include <thrust/iterator/zip_iterator.h>
#include "matop.h"
/*struct tensorDesc
{
	int nbDim;
	int* dim;
	tensorDesc();
	tensorDesc(int _nbDim);
	~tensorDesc();
	int nbElem() const;
};*/

enum activationType {Linear, Sigmoid, TanH};


//Interface for returning the states
//describes h
typedef std::vector<thrust::device_ptr<float>> RNNState;
//describes c
typedef thrust::device_ptr<float> MemState;


class layer_t
{
protected:

	int seqLength;
	int seqCounter;

	bool isInitialized;
	bool isTraining;
	int inputNeurons;
	int hiddenNeurons;
	int outputNeurons;
	int higherOrderStateSize;
	int stateVectorSize;
	int numLags;
	int numOrders;
	int batchSize;

	activationType activationOutput;
	activationType activationHidden;

	matop::matrix_t wx;
	matop::matrix_t wh;
	matop::matrix_t wy;
	matop::matrix_t by;

	thrust::device_vector<float> wx_data;
	thrust::device_vector<float> wh_data;
	thrust::device_vector<float> wy_data;
	thrust::device_vector<float> by_data;
	//weight Gradients
	matop::matrix_t dwx;
	matop::matrix_t dwh;
	matop::matrix_t dwy;
	matop::matrix_t dby;

	thrust::device_vector<float> dwx_data;
	thrust::device_vector<float> dwh_data;
	thrust::device_vector<float> dwy_data;
	thrust::device_vector<float> dby_data;
	//for RMSProp
	matop::matrix_t dwx_sq;
	matop::matrix_t dwh_sq;
	matop::matrix_t dwy_sq;
	matop::matrix_t dby_sq;

	thrust::device_vector<float> dwx_sq_data;
	thrust::device_vector<float> dwh_sq_data;
	thrust::device_vector<float> dwy_sq_data;
	thrust::device_vector<float> dby_sq_data;
	//for ADAM
	matop::matrix_t dwx_sq2;
	matop::matrix_t dwh_sq2;
	matop::matrix_t dwy_sq2;
	matop::matrix_t dby_sq2;

	thrust::device_vector<float> dwx_sq2_data;
	thrust::device_vector<float> dwh_sq2_data;
	thrust::device_vector<float> dwy_sq2_data;
	thrust::device_vector<float> dby_sq2_data;
	//inputData
	matop::matSequence_t* x;
	matop::matSequence_t* dx;
	matop::matSequence_t y;
	matop::matSequence_t dy;
	matop::matSequence_t h;
	matop::matSequence_t dh;

	thrust::device_vector<float> y_data;
	thrust::device_vector<float> dy_data;
	thrust::device_vector<float> h_data;
	thrust::device_vector<float> dh_data;

	matop::matSequence_t* stateTarget;
	matop::matSequence_t* dstateTarget;

	static matop::matrix_t s;
	static matop::matrix_t sgrad;
	static matop::matrix_t ds;
	static matop::matrix_t grad;
	static matop::matrix_t grad2;
	static matop::matrix_t stateTensor;

	static thrust::device_vector<float> d_ones;

	static thrust::device_vector<float> s_data;
	static thrust::device_vector<float> sgrad_data;
	static thrust::device_vector<float> ds_data;
	static thrust::device_vector<float> grad_data;
	static thrust::device_vector<float> grad2_data;
	static thrust::device_vector<float> stateTensor_data;

	static matop::matrix_t subTensors;
	static matop::matrix_t dsubTensors;

	static thrust::device_vector<float> subTensors_data;
	static thrust::device_vector<float> dsubTensors_data;

	static std::vector<matop::matrix_t> subResults;
	static matop::matSequence_t hLSTMTemp;
	static matop::matrix_t hLSTMGrad;

	static std::vector<thrust::device_vector<float>> subResults_data;
	static thrust::device_vector<float> hLSTMTemp_data;
	static thrust::device_vector<float> hLSTMGrad_data;

	static cudaStream_t* streams;
	static const int streamCount = 5;


	virtual void InitLayerSpecific();


public:
	static void InitDataAndMatrix(const matop::shape_t& shape, thrust::device_vector<float>& data, matop::matrix_t& mat);
	static void InitDataAndMatrix(const matop::shape_t& shape, const int seqLength, thrust::device_vector<float>& data, matop::matSequence_t& tens);

	layer_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut, activationType _actTypeHidden);

	void Init(int _batchSize, matop::matSequence_t*& data, matop::matSequence_t*& gradData, bool isTraining, int _seqLength);
	void unInit();

	//virtual ~layer_t()=0;
	~layer_t();

	//Network pass
	virtual void ForwardInference(const cublasHandle_t& cublasHandle);
	void ForwardTraining(const cublasHandle_t& cublasHandle, int seqNum);
	virtual void BackwardTraining(const cublasHandle_t& cublasHandle);
	virtual void ForwardState(const cublasHandle_t& cublasHandle, const matop::matrix_t& stateInput, matop::matrix_t& stateOutput) = 0;
	virtual void BackwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& _s,const matop::matrix_t& stateGrad, matop::matrix_t& _sgrad) = 0;
	virtual void ActivationState(const cublasHandle_t& cublasHandle, matop::matrix_t& input, matop::matrix_t& output);
	virtual void ActivationStateGrad(const cublasHandle_t& cublasHandle, const matop::matrix_t& inputGrad, matop::matrix_t& outputGrad);

	//handling the state vector
	void CreateStateVector(matop::matrix_t& s, int seqNum);
	void ScatterStateVector(const cublasHandle_t& cublasHandle, matop::matrix_t& _ds, int seqNum);

	//interface for states
	RNNState getLastStates(int seqNum, int amount);
	RNNState getLastStatesGrad(int seqNum, int amount);
	void setInitialState(const RNNState state);
	void setInitialStateGrad(const RNNState state, int seqNum);

	//functions are only used for lstm
	virtual MemState getLastMemory(int seqNum){return MemState();};
	virtual MemState getLastMemoryGrad(int seqNum){return MemState();};
	virtual void setInitialMemory(const MemState state){};
	virtual void setInitialMemoryGrad(const MemState state){};

	virtual void resetStates();
	virtual void resetStateGrad();
	//
	void setInput(matop::matSequence_t* inData, matop::matSequence_t* inDataGrad);



	//calls an Optimization strategy
	void UpdateWeights(float learningRate, int stepNum);
	//Optimization strategies
	void RMSProp(float learningRate);
	void ADAM(float learningRate, int stepNum);
	void SGD(float learningRate, float momentum);

	//Training Functions
	static void CopyBias(const matop::matrix_t& bias, matop::matrix_t& _y, int batch );
	static void AddBias(const cublasHandle_t& cublasHandle, const matop::matrix_t& bias, matop::matrix_t& _y, int batch );
	static void Activation(const thrust::device_ptr<float> start, size_t elements, thrust::device_ptr<float> result, activationType _actType );
	static void ActivationGradient(const thrust::device_ptr<float> hiddenState, const thrust::device_ptr<float> hiddenStateGrad, int size, activationType act);
	static void Reduce2D(	const cublasHandle_t& cublasHandle, cublasOperation_t transa,
				const float alpha,
				const matop::matrix_t& A,
				const float beta,
				matop::matrix_t& result);

	//shared memory across the functions
	static void initStaticHelpers(int numLags, int hiddenNeurons, int batchSize, int numOrders, bool RNN, bool RNNTT, bool LSTMTT, std::vector<int> TT_ranks = std::vector<int>());
	static void destroyHelper();
	static size_t GetStaticMemInfo();

	size_t GetMemInfo();

	//for tests
	void SetWeights(matop::matrix_t _Wx, matop::matrix_t _Wy, matop::matrix_t _Wh, matop::matrix_t _by  );
	void GetWeightGrad(matop::matrix_t& _Wx, matop::matrix_t& _Wy, matop::matrix_t& _Wh );


};

#endif
