/*
 * CUBLAS.h
Contains wrappers for CublasFunctions to use C-Style arrays more easily
 *
 *  Created on: Jun 10, 2020
 *      Author: julian
 */
#ifndef MATOP_H
#define MATOP_H
#include <cublas_v2.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <numeric>
#include <vector>
namespace matop
{
	typedef std::vector<int> shape_t;
	struct matrix_t
	{
	protected:
		thrust::device_ptr<float> dataptr;
		thrust::device_ptr<float> endptr;
	public:
		int rows;
		int cols;
		long long stride;
		int ld; //leading dimension
		matrix_t(): rows(0), cols(0), ld(0), stride(0), dataptr(thrust::device_ptr<float>()){};
		matrix_t(thrust::device_ptr<float> _data, int _rows, int _cols) :dataptr(_data), endptr(_data+_rows*_cols), rows(_rows), cols(_cols), ld(_cols), stride(_rows*_cols){};
		matrix_t(thrust::device_ptr<float> _data, int _rows, int _cols, long long int _ld) :dataptr(_data),endptr(_data+_rows*_cols), rows(_rows), cols(_cols), ld(_ld), stride(_rows*_ld){};
		matrix_t(thrust::device_ptr<float> _data, int _rows, int _cols, int _ld, long long int _stride) :dataptr(_data),endptr(_data+_rows*_cols), rows(_rows), cols(_cols), ld(_ld), stride(_stride){};

		float* ptr() const
		{
			return thrust::raw_pointer_cast(dataptr);
		}
		typename thrust::device_vector<float>::iterator begin() const
		{
			//assert(vec.size() > 0);
				return dataptr;
		}
		typename thrust::device_vector<float>::iterator end() const
		{
				//assert(vec.size() > 0);
				assert(ld == cols); //operation only makes sense for full matrix
				return endptr;
		}
		size_t size() const
		{
			return rows*cols;
		}
		thrust::device_ptr<float> data() const
		{
			return dataptr;
		}
		thrust::device_ptr<float>& data()
		{
					return dataptr;
		}
		bool empty() const
		{
			return (rows*cols == 0);
		}
	};

	struct matSequence_t
	{
	private:
			thrust::device_ptr<float> dataptr;
			thrust::device_ptr<float> endptr;
			std::vector<matrix_t> mat_array;
	public:
		int length;
		int sizePerElem;
		shape_t shape;
		matSequence_t() = default;
		matSequence_t(thrust::device_ptr<float> start, shape_t dims, int _length):
			sizePerElem(std::accumulate(dims.begin(),dims.end(),1,std::multiplies<int>())), shape(dims), length(_length)
		{
			this->dataptr = start;
			endptr = dataptr+length*sizePerElem;
			//this->vec = thrust::device_vector<T>(sizePerElem*length,0);
			for (int i = 0; i<length; ++i)
			{
				mat_array.push_back(matrix_t(dataptr+i*sizePerElem, shape[0], shape[1]));
			}
		}
		matrix_t& operator[](int idx)
		{
			int ret = idx % length;
			return mat_array[(ret>=0)?(ret):(ret+length)];
		}
		size_t size() const
		{
			return length*sizePerElem;
		}
		typename thrust::device_vector<float>::iterator begin()
				{
					return dataptr;
				}
		typename thrust::device_vector<float>::iterator end()
		{
			return endptr;
		}
		void clear()
		{
			mat_array.clear();
		}
		thrust::device_ptr<float> data() const
				{
					return dataptr;
				}
				thrust::device_ptr<float>& data()
				{
							return dataptr;
				}

	};
	void MatAdd(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const int m, const int n,
				const float alpha,
				const thrust::device_ptr<float>& A, const int Ald,
				const thrust::device_ptr<float>& B, const int Bld,
				const float beta,
				thrust::device_ptr<float> C, const int Cld);

	void MatAdd(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
			const float alpha,
			const matrix_t& A,
			const matrix_t& B,
			const float beta,
			matrix_t& C);

	void GEMM(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const float           alpha,
				const matrix_t& 	A,
				const matrix_t& 	B,
				const float           beta,
				matrix_t&           C);
	void GEMM_StridedBatched(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
				const float           alpha,
				const matrix_t& A,
				const matrix_t& B,
				const float           beta,
				matrix_t&     C,
				const int batchSize);
	void GEMM_StridedBatched(const cublasHandle_t& cublasHandle, cublasOperation_t transa, cublasOperation_t transb,
			const float           alpha, const int m, const int k, const int n,
			const thrust::device_ptr<float> A, const int lda, const int stridea,
			const thrust::device_ptr<float> B, const int ldb, const int strideb,
			const float           beta,
			thrust::device_ptr<float>       C, const int ldc, const int stridec,
			const int batchSize);
	//with explicit leading dimension

	void Reduce2D(	const cublasHandle_t& cublasHandle, cublasOperation_t transa,
			const float alpha,
			const matrix_t& A,
			const float beta,
			matrix_t& result);
	void Transpose( const cublasHandle_t& cublasHandle, const matrix_t& input, matrix_t& output);
	void Transpose( const cublasHandle_t& cublasHandle,
			const int rows, const int cols,
			const thrust::device_ptr<float>& input,
			const thrust::device_ptr<float>& output);
}
#endif


