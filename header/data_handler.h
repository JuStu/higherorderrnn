#ifndef DATA_HANDLER_H_
#define DATA_HANDLER_H_
#include "cnpy.h"
#include <string>
#include <stdexcept>
#include <list>
#include <thrust/device_vector.h>

enum setType {Training, Validation, Test};

class data_handler
{
private:
    /* data */
    unsigned int batch_size;
    int inp_steps;
    int out_steps;
    std::map<setType,	std::vector<std::vector<float>>> data;
    thrust::device_vector<float> data_prepare;
    std::map<setType, unsigned int> size;
    std::map<setType, unsigned int> currIdx;
    std::vector<float> temparr;

    std::vector<float> min;
    std::vector<float> max;

    cudaStream_t stream;

    unsigned int dataDim;
    unsigned int seqLength;
    template <typename T>
    void split_data_sets(cnpy::NpyArray arr);
    static cnpy::NpyArray convertToFloat(cnpy::NpyArray arr);
    template <typename T>
    static cnpy::NpyArray FortranToC(cnpy::NpyArray arr);

    void get_maxmin(std::vector<float>& currMax, std::vector<float>& currMin, setType set);
    void normalizeSet(setType set);
    std::vector<std::vector<float>> deNormalizeData(std::vector<std::vector<float>> data);
public:
    data_handler(std::string filename, unsigned int batch_s);
    void normalize();
    void savePrediction(void* inData, int inLength, void* predData, int outLength, std::string filename, int _batchSize);
    bool next_batch(void* dst, unsigned int seed, setType type);
    std::vector<unsigned int> get_shape() const;
    std::vector<unsigned int> get_setsizes() const;
    size_t get_wordsize() const;
    ~data_handler(){}
    
};

#endif
