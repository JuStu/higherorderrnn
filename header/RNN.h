/*
 * RNN.h
 *
 *  Created on: Jul 9, 2020
 *      Author: julian
 */

#ifndef RNN_H_
#define RNN_H_
#include "layer.h"
class RNN_t:public layer_t
{
    private:
        //void InitTraining() override;
        //void InitInference() override;
    public:
		RNN_t(int _inputNeurons, int _hiddenNeurons, int _outputNeurons,  int _numLags, int _numOrders, activationType _actTypeOut , activationType _actTypeHid);
        void CreateStateTensor(const cublasHandle_t& cublasHandle, const matop::matrix_t& _s, matop::matrix_t& _stateTensor);
        void StateGradient(const cublasHandle_t& cublasHandle,const matop::matrix_t& stateTensorGrad,const matop::matrix_t& _s, int seqNum);

        virtual void ForwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& stateInput, matop::matrix_t& stateOutput) override;
        virtual void BackwardState(const cublasHandle_t& cublasHandle,const matop::matrix_t& _s,const matop::matrix_t& stateGrad, matop::matrix_t& _sgrad) override;

};



#endif /* RNN_H_ */
