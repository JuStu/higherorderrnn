# HORNN

Implementierung eines RNN Höherer Ordnung für CUDA mit Tensor Train Faktorisierung

Beispiel:
./main inputfile.npy outputname -l 2 -o 2 -b 50 -h 8 -s 8 -r 2 -m 20000 -a 0.001 -x 5 -n

- -l: Anzahl der Layer, numLayers
- -o: Ordnung, numOrders
- -b: Größe des Batches, batchSize
- -h: Anzahl der historischen Daten, numLags
- -s: Anzahl der Neuronen, hiddenNeurons
- -r: Tensor Train rank
- -m: Anzahl der Trainingsschritte, default = 20000
- -x: Länge der Inputsequenz
- -y: Länge der Outputsequenz, default = Sequenzlänge - Inputsequenzlänge
- -d: Schritte bis zur Anzeige, default = 500
- -a: Lernrate
- -n: LSTM Flag

