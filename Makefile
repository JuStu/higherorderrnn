CXX = g++
NVCC = /usr/local/cuda-9.2/bin/nvcc -ccbin g++
CXXFLAGS = -Wall -Wextra -pedantic -pedantic-errors -O0 -std=c++14 #-03 optimize
COPTIONS = --compiler-options -Wall,-Wextra,-pedantic,-pedantic-errors,-O0,-std=c++14
#INCLUDE = -I/usr/local/cuda/include -I.
INCLUDE = -I header/ -I/usr/local/cuda-9.2/include 
#LIBRARIES = -L. -lcublas -lcudnn -lcudart -lstdc++ -lm 
CUDALIBS = -lcudart -lcublas -lcudnn
DEBUG = #-g -lineinfo
all: test
main: build/data_handler.o build/network.o build/layer.o build/main.o build/cnpy.o build/helper.o build/matop.o build/RNN.o build/TTRNN.o build/TTLSTM.o
	$(NVCC) $(COPTIONS) $(INCLUDE) $(CUDALIBS) $(DEBUG) build/cnpy.o build/data_handler.o build/network.o build/layer.o build/helper.o build/matop.o build/RNN.o build/TTRNN.o build/TTLSTM.o build/main.o -o main -lz
test: build/data_handler.o build/network.o build/layer.o build/test.o build/cnpy.o build/helper.o build/matop.o build/RNN.o build/TTRNN.o build/TTLSTM.o
	$(NVCC) $(COPTIONS) $(INCLUDE) $(CUDALIBS) $(DEBUG) build/cnpy.o build/data_handler.o build/network.o build/layer.o build/helper.o build/matop.o build/RNN.o build/TTRNN.o build/TTLSTM.o build/test.o -o tests/test -lz
build/test.o: tests/test.cu
	$(NVCC) $(INCLUDE) $(DEBUG) -c tests/test.cu -o build/test.o
build/main.o: src/main.cu
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/main.cu -o build/main.o
build/data_handler.o: src/data_handler.cu header/data_handler.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/data_handler.cu -o build/data_handler.o
build/cnpy.o: src/cnpy.cpp header/cnpy.h
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(DEBUG) -c src/cnpy.cpp -o build/cnpy.o
build/network.o: src/network.cu header/network.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/network.cu -o build/network.o
build/layer.o: src/layer.cu header/layer.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/layer.cu -o build/layer.o
build/RNN.o: src/RNN.cu header/RNN.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/RNN.cu -o build/RNN.o
build/TTRNN.o: src/TTRNN.cu header/TTRNN.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/TTRNN.cu -o build/TTRNN.o
build/TTLSTM.o: src/TTLSTM.cu header/TTLSTM.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/TTLSTM.cu -o build/TTLSTM.o
build/helper.o: src/helper.cu header/helper.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/helper.cu -o build/helper.o
build/matop.o: src/matop.cu header/matop.h
	$(NVCC) $(INCLUDE) $(DEBUG) -c src/matop.cu -o build/matop.o
clean:
	rm tests/test
	rm build/*.o
